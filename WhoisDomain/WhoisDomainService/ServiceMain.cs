﻿using WhoisDomain.Code.TaskProcess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace WhoisDomainService
{
    public partial class ServiceMain : ServiceBase
    {
        TaskMain TaskMain = null;

        public ServiceMain()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                TaskMain = new TaskMain();
                TaskMain.Start();
            }
            catch(Exception)
            {

            }
        }

        protected override void OnStop()
        {
            try
            {
                if (TaskMain != null)
                {
                    TaskMain.Stop();
                    TaskMain = null;
                }
            }
            catch(Exception)
            {

            }
        }
    }
}
