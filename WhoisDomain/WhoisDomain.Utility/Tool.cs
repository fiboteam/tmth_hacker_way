﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Globalization;

namespace WhoisDomain.Utility
{
    public class Tool
    {
        public static bool ChangeParamAppConfig(string key, string value, string parent = "appSettings")
        {
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.AppSettings.Settings[key].Value = value;
                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static string GetParamAppConfig(string key)
        {
            try
            {
                return ConfigurationManager.AppSettings.Get(key);
            }
            catch(Exception)
            {
                return String.Empty;
            }
        }

        public static DateTime? ParseStringToDateTime(string dateTime)
        {
            try
            {
                string[] pattern = new string[] 
                { 
                    "d/M/yyyy",
                    "d/M/yyyy HH:mm:ss",
                    "d/M/yyyy hh:mm:ss tt",
                    "dd/MM/yyyy",
                    "dd/MM/yyyy HH:mm:ss",
                    "dd/MM/yyyy hh:mm:ss tt",
                    "yyyy/M/d",
                    "yyyy/M/d HH:mm:ss",
                    "yyyy/M/d hh:mm:ss tt",
                    "yyyy/MM/dd",
                    "yyyy/MM/dd HH:mm:ss",
                    "yyyy/MM/dd hh:mm:ss tt",
                    "M/d/yyyy",
                    "M/d/yyyy HH:mm:ss",
                    "M/d/yyyy hh:mm:ss tt",
                    "MM/dd/yyyy",
                    "MM/dd/yyyy HH:mm:ss",
                    "MM/dd/yyyy hh:mm:ss tt",
                };

                string[] arrParms = dateTime.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                if (arrParms.Length > 0)
                {
                    arrParms[0] = arrParms[0].Replace("-", "/");
                    string rawDateTime = arrParms[0];

                    if (arrParms.Length > 1)
                    {
                        arrParms[1] = arrParms[1].Replace("/", ":");
                        arrParms[1] = arrParms[1].Replace("-", ":");
                        rawDateTime += " " + arrParms[1];

                        if (arrParms.Length > 2)
                        {
                            rawDateTime += " " + arrParms[2];
                        }
                    }

                    DateTime resultDateTime = DateTime.ParseExact(rawDateTime, pattern, CultureInfo.CurrentCulture, DateTimeStyles.None);
                    return resultDateTime;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
