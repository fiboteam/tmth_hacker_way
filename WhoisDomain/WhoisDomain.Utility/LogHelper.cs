﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using System.Configuration;
using System.Net.Mail;
using System.Net;

namespace WhoisDomain.Utility
{
    /// <summary>
    /// Hỗ trợ dùng Log4net.
    /// </summary>
    public class LogHelper
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(LogHelper));
        
        public static void WriteLog(string content)
        {
            try
            {
                logger.Info(content);
            }
            catch(Exception)
            {

            }
        }
    }
}
