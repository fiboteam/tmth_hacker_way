﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WhoisDomain.Utility
{
   public enum StatusWhois: short
   {
       Registered = 1,
       NotFound = 2,
   }
}
