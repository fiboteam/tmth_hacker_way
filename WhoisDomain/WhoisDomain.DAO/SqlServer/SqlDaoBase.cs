﻿using WhoisDomain.Objects;
using WhoisDomain.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data;
using System.Reflection;
using System.ComponentModel;
using System.IO;

namespace WhoisDomain.DAO
{
    public abstract class SqlDaoBase<T> where T : BusinessObject
    {
        private string mTableName;
        private string mEntityIDName;
        private string mStoreProcedurePrefix;

        public string TableName
        {
            get { return this.mTableName; }
            set { this.mTableName = value; }
        }

        public string EntityIDName
        {
            get { return this.mEntityIDName; }
            set { this.mEntityIDName = value; }
        }

        public string StoreProcedurePrefix
        {
            get { return this.mStoreProcedurePrefix; }
            set { this.mStoreProcedurePrefix = value; }
        }

        public SqlDaoBase() { }

        public SqlDaoBase(string tableName, string entityIDName, string storeProcedurePrefix)
        {
            mTableName = tableName;
            mEntityIDName = entityIDName;
            mStoreProcedurePrefix = storeProcedurePrefix;
        }

        public virtual bool Insert(T businessObject)
        {
            string sql = mStoreProcedurePrefix + "AddUpdate";
            businessObject.ID = DbAdapter1.ExcecuteScalar(sql, true, Take(businessObject)).AsLong();

            if (businessObject.ID > 0)
                return true;
            else
                return false;
        }

        public virtual bool Update(T businessObject)
        {
            string sql = mStoreProcedurePrefix + "AddUpdate";
            businessObject.ID = DbAdapter1.ExcecuteScalar(sql, true, Take(businessObject)).AsLong();

            if (businessObject.ID > 0)
                return true;
            else
                return false;
        }

        public virtual int Delete(long entityID)
        {
            string sql = string.Format(@"DELETE FROM {0} WHERE {1} = @ID", mTableName, mEntityIDName);
            object[] parms = { "@ID", entityID };

            return DbAdapter1.ExecuteNonQuery(sql, false, parms);
        }

        public virtual T GetSingle(long entityID)
        {
            try
            {
                string sql = string.Format(@"SELECT * FROM {0} WHERE {1} = @EntityID", mTableName, mEntityIDName);
                object[] parms = { "@EntityID", entityID };

                return DbAdapter1.Read(sql, Make, false, parms);
            }
            catch
            {
                return null;
            }
        }

        public virtual IList<T> GetAll()
        {
            try
            {
                string sql = string.Format(@"SELECT * FROM {0}", TableName).OrderBy(mEntityIDName + " desc");
                return DbAdapter1.ReadList(sql, Make, false);
            }
            catch
            {
                return null;
            }
        }

        public virtual int Count()
        {
            try
            {
                string sql = string.Format(@"SELECT count({0}) FROM {1}", mEntityIDName, TableName);
                return DbAdapter1.GetCount(sql, false, null);
            }
            catch
            {
                return -1;
            }
        }

        protected virtual object[] Take(T businessObject)
        {
            Type type = typeof(T);
            List<object> listProperties = new List<object>();
            foreach (PropertyInfo info in type.GetProperties())
            {
                if (info.MemberType == MemberTypes.Property && info.PropertyType != typeof(Dictionary<string, object>)
                    && info.PropertyType.FullName.IndexOf("BusinessObject") == -1 && info.Name != "EcryptedID")
                {
                    string fieldName = info.Name;
                    if (fieldName == "ID")
                        fieldName = mEntityIDName;
                    object obj = new object();
                    obj = "@" + fieldName;
                    listProperties.Add(obj);

                    object propValue = new object();
                    if (info.PropertyType.BaseType.ToString().IndexOf("Enum") >= 0)
                    {
                        propValue = Enum.Parse(info.GetValue(businessObject, null).GetType(), info.GetValue(businessObject, null).ToString()).AsShort();
                    }
                    else
                    {
                        propValue = info.GetValue(businessObject, null);
                    }
                    listProperties.Add(propValue);
                }
            }
            return listProperties.ToArray();
        }

        protected virtual T Make(IDataReader reader)
        {
            Type type = typeof(T);
            T entity = (T)Activator.CreateInstance(type);

            for (int i = 0; i < reader.FieldCount; i++)
            {
                string column = reader.GetName(i).ToUpper();
                bool check = true;

                if (column != null && column != "")
                {
                    foreach (PropertyInfo info in type.GetProperties())
                    {
                        try
                        {
                            string fieldName = info.Name;
                            if (fieldName == "ID")
                                fieldName = mEntityIDName;
                            if (column == fieldName.ToUpper())
                            {
                                info.SetValue(entity, TypeDescriptor.GetConverter(info.PropertyType).ConvertFrom(reader[fieldName].ToString()), null);
                                check = false;
                            }
                        }
                        catch { }
                    }
                    if (check)
                    {
                        entity.ExtenstionProperties.Add(column, reader[i].AsString());
                    }
                }
            }

            return (T)entity;
        }
    }
}