﻿using WhoisDomain.Objects;
using WhoisDomain.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WhoisDomain.DAO.SqlServer
{
    public class SqlDomainVNDao : SqlDaoBase<tblDomainVN>
    {
        public SqlDomainVNDao()
        {
            TableName = "tblDomainVN";
            EntityIDName = "DomainID";
            StoreProcedurePrefix = "spDomainVN_";
        }

        public SqlDomainVNDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }

        public List<tblDomainVN> GetDomainVN_WhoisNormal(long amountRecord, int beforeExpireDate, int afterExpireDate, string storeProcedureName = null)
        {
            try
            {
                string sql = StoreProcedurePrefix + (String.IsNullOrEmpty(storeProcedureName) ? "GetDomainVN_WhoisNormal" : storeProcedureName);

                var parms = new object[] { 
                    "@AmountRecord", amountRecord, 
                    "@BeforeExpireDate", beforeExpireDate,
                    "@AfterExpireDate", afterExpireDate
                };

                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch(Exception)
            {
                return null;
            }
        }

        public List<tblDomainVN> GetDomainVN_WhoisHasExpire(long amountRecord, int afterExpireDate, string storeProcedureName = null)
        {
            try
            {
                string sql = StoreProcedurePrefix + (String.IsNullOrEmpty(storeProcedureName) ? "GetDomainVN_WhoisHasExpire" : storeProcedureName);

                var parms = new object[] { 
                    "@AmountRecord", amountRecord, 
                    "@AfterExpireDate", afterExpireDate
                };

                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool UpdateDomainVN_WhoisNormal(tblDomainVN item, string maxShortLastDateWhois)
        {
            try
            {
                if (item.ID <= 0)
                    return false;

                item.UpdatedDate = DateTime.Now;
                item.CreatedDate = DateTime.Now;
                item.ShortLastDateWhois = int.Parse(maxShortLastDateWhois);
                item.LastChecked = DateTime.Parse(
                    maxShortLastDateWhois.Substring(0, 4) + "-" +
                    maxShortLastDateWhois.Substring(4, 2) + "-" +
                    maxShortLastDateWhois.Substring(6, 2)
                    );

                string sql = StoreProcedurePrefix + "UpdateDomainVN_WhoisNormal";

                long result = DbAdapter1.ExcecuteScalar(sql, true, Take(item)).AsLong();

                if (result > 0)
                    return true;
                else
                    return false;
            }
            catch(Exception)
            {
                return false;
            }
        }
    }
}
