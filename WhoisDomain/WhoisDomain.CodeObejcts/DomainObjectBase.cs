﻿using WhoisDomain.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace WhoisDomain.CodeObjects
{
    public class DomainObjectBase
    {
        /// <summary>
        /// Tên miền
        /// </summary>
        public string DomainName { set; get; }
        /// <summary>
        /// Ngày đăng ký
        /// </summary>
        public DateTime RegistrationDate { set; get; }
        /// <summary>
        /// Ngày hết hạn
        /// </summary>
        public DateTime ExpireDate { set; get; }
        /// <summary>
        /// Ngày Whois
        /// </summary>
        public DateTime LastChecked { set; get; }
        /// <summary>
        /// Người/Công ty đăng ký
        /// </summary>
        public string Owner { set; get; }
        /// <summary>
        /// Công ty giữ tên miền (vd: Mắt Bảo, PA, VDC...)
        /// </summary>
        public string CurrentRegistrar { set; get; }
        /// <summary>
        /// Name Server
        /// </summary>
        public string DNSServer { set; get; }
        /// <summary>
        /// Web dùng để Whois
        /// </summary>
        public string WhoisServerName { set; get; }
        /// <summary>
        /// Ngày Whois cuối cùng
        /// </summary>
        public int ShortLastDateWhois { set; get; }
        public int ShortExpireDate { set; get; }

        public DomainObjectBase()
        {
            LastChecked = DateTime.Today;
            ShortLastDateWhois = int.Parse(LastChecked.ToString("yyyyMMdd"));
        }

        public bool AssignProperties(object source)
        {
            try
            {
                if (source == null)
                    return false;

                foreach (var i in this.GetType().GetProperties())
                {
                    PropertyInfo pInfo = source.GetType().GetProperty(i.Name);
                    if (pInfo != null)
                    {
                        i.SetValue(this, pInfo.GetValue(source, null), null);
                    }
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
