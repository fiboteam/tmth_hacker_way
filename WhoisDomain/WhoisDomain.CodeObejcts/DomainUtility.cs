﻿using WhoisDomain.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.ComponentModel;
using System.Globalization;

namespace WhoisDomain.CodeObjects
{
    public class DomainUtility
    {
        public static Dictionary<string, string> DeserializeResponseRaw(List<string> parms, string textResponse, string[] charRemove = null)
        {
            try
            {
                if (String.IsNullOrEmpty(textResponse))
                    return null;

                if (charRemove != null)
                {
                    foreach (var i in charRemove)
                    {
                        textResponse = textResponse.Replace(i, String.Empty);
                    }
                }

                bool getByNextParam = false;
                Dictionary<string, string> dicParms = new Dictionary<string, string>();

                if (parms.Count <= 0)
                    return null;

                string after = parms[0];
                //parms.RemoveAt(0);

                if (after.Equals("NextParam"))
                    getByNextParam = true;

                string before = "";
                string value = "";
                string valueTmp = "";
                for (int i = 1; i < parms.Count; i++)
                {
                    before = parms[i];

                    if (i == (parms.Count - 1))
                    {
                        value = textResponse.Substring(textResponse.IndexOf(before) + before.Length);
                        value = value.Replace("\r", "");
                        value = value.Replace("\n", "");
                        value = value.Replace("\t", "");
                        dicParms.Add(before, value.Trim());
                        break;
                    }

                    if (getByNextParam)
                        after = parms[i + 1];

                    valueTmp = textResponse.Substring(textResponse.IndexOf(before) + before.Length);
                    value = valueTmp.Substring(0, valueTmp.IndexOf(after)).Trim();
                    dicParms.Add(before, value);
                }

                return dicParms;
            }
            catch(Exception)
            {
                return null;
            }
        }

        public static string RemoveHtmlTags(string textResponse)
        {
            try
            {
                return Regex.Replace(textResponse, "<.*?>", String.Empty);
            }
            catch(Exception)
            {
                return textResponse;
            }
        }

        public static DomainObjectBase DeserializeResponse(Dictionary<string, string> dicParamsDomain, List<string> listParamsData)
        {
            try
            {
                if (dicParamsDomain == null || dicParamsDomain.Count <= 0)
                    return null;

                DomainObjectBase item = new DomainObjectBase();

                for (int i = 0; i < dicParamsDomain.Count; i++)
                {
                    var property = item.GetType().GetProperty(listParamsData[i]);
                    if (property == null)
                        continue;

                    var typeMember = property.PropertyType;
                    var typeConverter = TypeDescriptor.GetConverter(typeMember);
                    object value;

                    if (typeMember.Equals(typeof(DateTime)))
                    {
                        dicParamsDomain[dicParamsDomain.ElementAt(i).Key] = dicParamsDomain.ElementAt(i).Value.Replace(" 00", "");
                    }

                    value = typeConverter.ConvertFromString(null, CultureInfo.GetCultureInfoByIetfLanguageTag("vi"), dicParamsDomain.ElementAt(i).Value);
                    property.SetValue(item, value, null);
                }

                item.ShortExpireDate = int.Parse(item.ExpireDate.ToString("yyyyMMdd"));
                return item;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
