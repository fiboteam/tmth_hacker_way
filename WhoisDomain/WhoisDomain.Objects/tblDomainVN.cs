﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WhoisDomain.Objects
{
    public class tblDomainVN : BusinessObject
    {
        /// <summary>
        /// Tên miền
        /// </summary>
        public string DomainName { set; get; }
        /// <summary>
        /// Ngày đăng ký
        /// </summary>
        public DateTime RegistrationDate { set; get; }
        /// <summary>
        /// Ngày hết hạn
        /// </summary>
        public DateTime ExpireDate { set; get; }
        /// <summary>
        /// Ngày Whois
        /// </summary>
        public DateTime LastChecked { set; get; }
        /// <summary>
        /// Người/Công ty đăng ký
        /// </summary>
        public string Owner { set; get; }
        /// <summary>
        /// Công ty giữ tên miền (vd: Mắt Bảo, PA, VDC...)
        /// </summary>
        public string CurrentRegistrar { set; get; }
        /// <summary>
        /// Name Server
        /// </summary>
        public string DNSServer { set; get; }
        /// <summary>
        /// Web dùng để Whois
        /// </summary>
        public string WhoisServerName { set; get; }
        /// <summary>
        /// Ngày Whois cuối cùng
        /// </summary>
        public int ShortLastDateWhois { set; get; }
        public int ShortExpireDate { set; get; }

        public tblDomainVN()
        {
            LastChecked = DateTime.Today;
            ShortLastDateWhois = int.Parse(LastChecked.ToString("yyyyMMdd"));
        }
    }
}
