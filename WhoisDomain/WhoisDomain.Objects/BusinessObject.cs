﻿using WhoisDomain.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;

namespace WhoisDomain.Objects
{
    [DataContract]
    public abstract class BusinessObject
    {
        [DataMember]
        private Dictionary<string, object> dynamicProperties = new Dictionary<string, object>();

        [DataMember]
        public Dictionary<string, object> ExtenstionProperties
        {
            get
            {
                return dynamicProperties;
            }
            set
            {
                dynamicProperties = value;
            }
        }

        private long id = -1;

        [ScaffoldColumn(false)]
        [DataMember]
        [Required]
        public virtual long ID
        {
            get { return id; }
            set { id = value; }
        }

        public string EcryptedID
        {
            get { return GFunction.Encrypt(ID.ToString()); }
        }

        private DateTime createdDate = DateTime.Now;
        [DataMember]
        public DateTime CreatedDate
        {
            get { return this.createdDate; }
            set { this.createdDate = value; }
        }

        private DateTime updatedDate = DateTime.Now;
        [DataMember]
        public DateTime UpdatedDate
        {
            get { return this.updatedDate; }
            set { this.updatedDate = value; }
        }

        public bool AssignProperties(object source)
        {
            try
            {
                if (source == null)
                    return false;

                foreach (var i in this.GetType().GetProperties())
                {
                    PropertyInfo pInfo = source.GetType().GetProperty(i.Name);
                    if (pInfo != null)
                    {
                        i.SetValue(this, pInfo.GetValue(source, null), null);
                    }
                }

                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }
    }
}
