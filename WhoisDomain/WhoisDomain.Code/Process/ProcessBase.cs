﻿using WhoisDomain.CodeObjects;
using WhoisDomain.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WhoisDomain.Code.Process
{
    public abstract class ProcessBase
    {
        protected DateTime LastWhois { set; get; }
        protected int DelayTime { set; get; }
        protected bool FinishGet { set; get; }
        public StatusWhois StatusWhois { set; get; }

        public ProcessBase()
        {
            FinishGet = true;
            LastWhois = new DateTime(0);
            StatusWhois = Utility.StatusWhois.Registered;
        }

        public abstract DomainObjectBase Get(string domainName);
        public abstract bool WhoisDomain();
    }
}
