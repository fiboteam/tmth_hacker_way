﻿using WhoisDomain.Utility;
using WhoisDomain.Code;
using WhoisDomain.CodeObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading;

namespace WhoisDomain.Code.Process
{
    public class ProcessMatBao : ProcessBase
    {
        string MatBao_WhoisUrl = Tool.GetParamAppConfig("MatBao_WhoisUrl");
        string MatBao_NoRegister = Tool.GetParamAppConfig("MatBao_NoRegister");
        string MatBao_ReplaceCharDNS = Tool.GetParamAppConfig("MatBao_ReplaceCharDNS");
        string MatBao_Reserved = Tool.GetParamAppConfig("MatBao_Reserved");
        string[] MatBao_CharRemove = Tool.GetParamAppConfig("MatBao_CharRemove").Split('|');

        List<string> MatBao_ParamsData = new List<string>(Tool.GetParamAppConfig("MatBao_ParamsData").Split('|'));
        List<string> MatBao_Params = new List<string>(Tool.GetParamAppConfig("MatBao_Params").Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries));

        public ProcessMatBao()
        {
            DelayTime = int.Parse(Tool.GetParamAppConfig("MatBao_DelayTime"));
        }

        public string DomainName 
        {
            set 
            {
                value = value.ToLower();
                domain = value.Substring(0, value.IndexOf("."));
                domainExtension = "." + value.Substring(value.IndexOf(".") + 1);

                MatBao_WhoisUrl = Tool.GetParamAppConfig("MatBao_WhoisUrl").Replace("[domainname]", value);
                MatBao_NoRegister = Tool.GetParamAppConfig("MatBao_NoRegister").Replace("@@@", value);
                MatBao_Reserved = Tool.GetParamAppConfig("MatBao_Reserved").Replace("@@@", value);
            }
            get
            {
                return domain + domainExtension;
            }
        }

        DomainObjectBase DomainObjectBase;
        string domain = "";
        string domainExtension = "";

        public override DomainObjectBase Get(string domainName)
        {
            //Chờ Task Khác Get xong
            while (!FinishGet) { }
            FinishGet = false;

            while ((DateTime.Now - LastWhois).TotalSeconds <= DelayTime)
            {
                Thread.Sleep(1000);
            }

            DomainName = domainName;

            if (String.IsNullOrEmpty(domain) || String.IsNullOrEmpty(domainExtension))
            {
                FinishGet = true;
                LastWhois = DateTime.Now;
                return null;
            }

            if (WhoisDomain())
            {
                LastWhois = DateTime.Now;
                FinishGet = true;
                return DomainObjectBase;
            }

            FinishGet = true;
            LastWhois = DateTime.Now;
            return null;
        }

        public override bool WhoisDomain()
        {
            try
            {
                var webClient = new WebClient();
                webClient.Encoding = Encoding.UTF8;
                var textResponse = webClient.DownloadString(MatBao_WhoisUrl);
                textResponse = textResponse.Replace(MatBao_ReplaceCharDNS, " - ");
                textResponse = DomainUtility.RemoveHtmlTags(textResponse);

                var parms = DomainUtility.DeserializeResponseRaw(MatBao_Params, textResponse, MatBao_CharRemove);
                DomainObjectBase = DomainUtility.DeserializeResponse(parms, MatBao_ParamsData);

                if (DomainObjectBase == null)
                {
                    if (textResponse.Contains(MatBao_NoRegister))
                    {
                        //Tên miền chưa được đăng ký
                        StatusWhois = Utility.StatusWhois.NotFound;
                        return true;
                    }

                    if(textResponse.Contains(MatBao_Reserved))
                    {
                        //Được Vnnic giữ chỗ không được cấp phát
                        StatusWhois = Utility.StatusWhois.NotFound;
                        return true;
                    }
                }

                if (DomainObjectBase == null)
                    return false;

                DomainObjectBase.DomainName = DomainName;
                DomainObjectBase.WhoisServerName = "MatBao";
                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }
    }
}
