﻿using WhoisDomain.CodeObjects;
using WhoisDomain.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel;

namespace WhoisDomain.Code.Process
{
    public class ProcessVnnic : ProcessBase
    {
        string Vnnic_SelectName = Tool.GetParamAppConfig("Vnnic_SelectName");
        string Vnnic_InputName = Tool.GetParamAppConfig("Vnnic_InputName");
        string Vnnic_SubmitNam = Tool.GetParamAppConfig("Vnnic_SubmitNam");
        string Vnnic_UrlMain = Tool.GetParamAppConfig("Vnnic_UrlMain");
        string Vnnic_UrlCheck = Tool.GetParamAppConfig("Vnnic_UrlCheck");
        string Vnnic_LinkDetail = Tool.GetParamAppConfig("Vnnic_LinkDetail");
        string Vnnic_UrlDetail = Tool.GetParamAppConfig("Vnnic_UrlDetail");
        string Vnnic_NoRegister = Tool.GetParamAppConfig("Vnnic_NoRegister");
        string Vnnic_DontAllocate = Tool.GetParamAppConfig("Vnnic_DontAllocate");
        string Vnnic_Reserved = Tool.GetParamAppConfig("Vnnic_Reserved");

        int Vnnic_TimeWait = int.Parse(Tool.GetParamAppConfig("Vnnic_TimeWait"));
        int TimeTryWhoisProcess = int.Parse(Tool.GetParamAppConfig("TimeTryWhoisProcess"));
        List<string> Vnnic_ParamsData = new List<string>(Tool.GetParamAppConfig("Vnnic_ParamsData").Split('|'));
        List<string> Vnnic_Params = new List<string>(Tool.GetParamAppConfig("Vnnic_Params").Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries));

        public string DomainName 
        {
            set 
            {
                value = value.ToLower();
                domain = value.Substring(0, value.IndexOf("."));
                domainExtension = "." + value.Substring(value.IndexOf(".") + 1);

                Vnnic_NoRegister = Tool.GetParamAppConfig("Vnnic_NoRegister").Replace("@@@", value);
                Vnnic_DontAllocate = Tool.GetParamAppConfig("Vnnic_DontAllocate").Replace("@@@", value);
                Vnnic_Reserved = Tool.GetParamAppConfig("Vnnic_Reserved").Replace("@@@", value);
            }
            get { return domain + domainExtension; }
        }

        string domain = "";
        string domainExtension = "";
        public DomainObjectBase DomainObjectBase;
        WebBrowser _webBrowser;
        bool? resultWhois = null;
        AutoResetEvent webResponse;
        string urlCurrent = "";
        DateTime timeWait = DateTime.Now;
        public bool Finish = false;

        public ProcessVnnic()
        {
            webResponse = new AutoResetEvent(false);
            DelayTime = int.Parse(Tool.GetParamAppConfig("Vnnic_DelayTime"));
        }

        public ProcessVnnic(string domainName)
        {
            webResponse = new AutoResetEvent(false);
            DomainName = domainName;
        }

        public void LoadControl()
        {
            if (_webBrowser != null)
                _webBrowser.Dispose();

            _webBrowser = new WebBrowser();
            _webBrowser.AllowNavigation = true;
            _webBrowser.ScriptErrorsSuppressed = true;
            _webBrowser.DocumentCompleted += _webBrowser_DocumentCompleted;
        }

        void ReadWebResponse()
        {
            try
            {
                if (urlCurrent.Contains(Vnnic_UrlMain))
                {
                    var select = _webBrowser.Document.GetElementsByTagName("option");

                    for (int i = 0; i < select.Count; i++)
                    {
                        if (!select[i].Parent.Name.Equals(Vnnic_SelectName))
                            continue;

                        if (!select[i].InnerText.Equals(domainExtension))
                        {
                            select[i].SetAttribute("selected", "");
                        }
                        else
                        {
                            select[i].SetAttribute("selected", "selected");
                        }
                    }

                    var inputDomain = _webBrowser.Document.GetElementsByTagName("input");

                    for (int i = 0; i < inputDomain.Count; i++)
                    {
                        if (inputDomain[i].Name.Equals(Vnnic_InputName))
                        {
                            inputDomain[i].SetAttribute("value", domain);
                            break;
                        }
                    }

                    var formElements = _webBrowser.Document.GetElementsByTagName("input");

                    for (int i = 0; i < formElements.Count; i++)
                    {
                        if (formElements[i].Name.Equals(Vnnic_SubmitNam))
                        {
                            formElements[i].InvokeMember("click");
                            break;
                        }
                    }
                }

                //Tìm xem kết quả trả về domain có tồn tại hay không
                if (urlCurrent.Contains(Vnnic_UrlCheck))
                {
                    var linkElement = _webBrowser.Document.GetElementsByTagName("a");
                    for (int i = 0; i < linkElement.Count; i++)
                    {
                        var tmp = linkElement[i].GetAttribute("href");
                        if (linkElement[i].GetAttribute("href").Contains(Vnnic_LinkDetail))
                        {
                            linkElement[i].InvokeMember("click");
                            break;
                        }
                    }

                    if (resultWhois == null)
                    {
                        if (_webBrowser.Document.Body.InnerText.Contains(Vnnic_NoRegister))
                        { 
                            //Tiền miền chưa được đăng ký
                            StatusWhois = Utility.StatusWhois.NotFound;
                        }

                        if (_webBrowser.Document.Body.InnerText.Contains(Vnnic_Reserved))
                        {
                            //Được Vnnic giữ chỗ không được cấp phát
                            StatusWhois = Utility.StatusWhois.NotFound;
                        }

                        if (_webBrowser.Document.Body.InnerText.Contains(Vnnic_DontAllocate))
                        {
                            //thuộc danh mục tên miền cấp 2 theo địa giới hành chính và tên miền cấp 2 chung, không xét cấp phát
                            StatusWhois = Utility.StatusWhois.NotFound;
                        }

                        resultWhois = true;
                    }
                }

                //Submit thành công vào page chi tiết domain
                if (urlCurrent.Contains(Vnnic_UrlDetail))
                {
                    string textResponse = _webBrowser.Document.Body.InnerText;
                    var parms = DomainUtility.DeserializeResponseRaw(Vnnic_Params, textResponse);

                    DomainObjectBase = null;
                    DomainObjectBase = DomainUtility.DeserializeResponse(parms, Vnnic_ParamsData);

                    if (DomainObjectBase == null)
                        resultWhois = false;
                    else
                    {
                        DomainObjectBase.DomainName = DomainName;
                        DomainObjectBase.WhoisServerName = "Vnnic";
                        resultWhois = true;
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        void _webBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            timeWait = DateTime.Now;
            urlCurrent = e.Url.AbsoluteUri;
            ReadWebResponse();
        }

        public void Start()
        {
            Task.Factory.StartNew(() =>
            {
                var threadVnnic = new Thread(() =>
                {
                    WhoisDomain();
                });

                threadVnnic.SetApartmentState(ApartmentState.STA);
                threadVnnic.Start();
            });

            while (resultWhois == null) { }
        }

        public override bool WhoisDomain()
        {
            try
            {
                int timeTry = 0;
                do
                {
                    LoadControl();
                    _webBrowser.Navigate(Vnnic_UrlMain);

                    while (resultWhois == null)
                    {
                        Application.DoEvents(); //Phải có mới chạy được Event _webBrowser_DocumentCompleted
                        if ((DateTime.Now - timeWait).TotalSeconds >= Vnnic_TimeWait)
                            break;
                    }

                    if (resultWhois != null)
                    {
                        Finish = true;
                        return resultWhois.Value;
                    }

                } while ((timeTry++) < TimeTryWhoisProcess); //Thử lại
            }
            catch (Exception)
            {

            }

            resultWhois = false;
            return false;
        }

        public override DomainObjectBase Get(string domainName)
        {
            //Chờ Task Khác Get xong
            while (!FinishGet) { }
            FinishGet = false;

            while ((DateTime.Now - LastWhois).TotalSeconds <= DelayTime)
            {
                Thread.Sleep(1000);
            }

            DomainName = domainName;

            if (String.IsNullOrEmpty(domain) || String.IsNullOrEmpty(domainExtension))
            {
                FinishGet = true;
                LastWhois = DateTime.Now;
                return null;
            }

            Start();
            if (resultWhois.Value)
            {
                FinishGet = true;
                LastWhois = DateTime.Now;
                return DomainObjectBase;
            }

            FinishGet = true;
            LastWhois = DateTime.Now;
            return null;
        }
    }
}

