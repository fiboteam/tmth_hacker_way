﻿using WhoisDomain.Utility;
using WhoisDomain.Code;
using WhoisDomain.CodeObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading;

namespace WhoisDomain.Code.Process
{
    public class ProcessNhanHoa: ProcessBase
    {
        string NhanHoa_WhoisUrl = Tool.GetParamAppConfig("NhanHoa_WhoisUrl");
        string NhanHoa_CheckDomain = Tool.GetParamAppConfig("NhanHoa_CheckDomain");
        string[] NhanHoa_CharRemove = Tool.GetParamAppConfig("NhanHoa_CharRemove").Split('|');

        List<string> NhanHoa_ParamsData = new List<string>(Tool.GetParamAppConfig("NhanHoa_ParamsData").Split('|'));
        List<string> NhanHoa_Params = new List<string>(Tool.GetParamAppConfig("NhanHoa_Params").Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries));

        public ProcessNhanHoa()
        {
            DelayTime = int.Parse(Tool.GetParamAppConfig("NhanHoa_DelayTime"));
        }

        public string DomainName 
        {
            set 
            {
                value = value.ToLower();
                domain = value.Substring(0, value.IndexOf("."));
                domainExtension = "." + value.Substring(value.IndexOf(".") + 1);

                NhanHoa_WhoisUrl = Tool.GetParamAppConfig("NhanHoa_WhoisUrl");
                NhanHoa_WhoisUrl = NhanHoa_WhoisUrl.Replace("[domain]", domain);
                NhanHoa_WhoisUrl = NhanHoa_WhoisUrl.Replace("[domainextension]", domainExtension);

                NhanHoa_CheckDomain = Tool.GetParamAppConfig("NhanHoa_CheckDomain");
                NhanHoa_CheckDomain = NhanHoa_CheckDomain.Replace("[domain]", domain);
                NhanHoa_CheckDomain = NhanHoa_CheckDomain.Replace("[domainextension]", domainExtension);
            }
            get
            {
                return domain + domainExtension;
            }
        }

        DomainObjectBase DomainObjectBase;
        string domain = "";
        string domainExtension = "";

        public override DomainObjectBase Get(string domainName)
        {
            //Chờ Task Khác Get xong
            while (!FinishGet) { }
            FinishGet = false;

            while ((DateTime.Now - LastWhois).TotalSeconds <= DelayTime)
            {
                Thread.Sleep(1000);
            }

            DomainName = domainName;
            if (String.IsNullOrEmpty(domain) || String.IsNullOrEmpty(domainExtension))
            {
                LastWhois = DateTime.Now;
                FinishGet = true;
                return null;
            }

            if (WhoisDomain())
            {
                LastWhois = DateTime.Now;
                FinishGet = true;
                return DomainObjectBase;
            }

            LastWhois = DateTime.Now;
            FinishGet = true;
            return null;
        }

        public override bool WhoisDomain()
        {
            try
            {
                var webClient = new WebClient();
                webClient.Encoding = Encoding.UTF8;
                var textResponse = webClient.DownloadString(NhanHoa_WhoisUrl);
                textResponse = DomainUtility.RemoveHtmlTags(textResponse);

                var parms = DomainUtility.DeserializeResponseRaw(NhanHoa_Params, textResponse, NhanHoa_CharRemove);
                DomainObjectBase = DomainUtility.DeserializeResponse(parms, NhanHoa_ParamsData);

                if (DomainObjectBase == null)
                {
                    textResponse = webClient.DownloadString(NhanHoa_CheckDomain);
                    if (textResponse.Equals("0"))
                    {
                        //Tên miền chưa được đăng ký
                        StatusWhois = Utility.StatusWhois.NotFound;
                        return true;
                    }
                    else if(textResponse.Equals("1"))
                    {
                        //Được Vnnic giữ chỗ không được cấp phát
                        //thuộc danh mục tên miền cấp 2 theo địa giới hành chính và tên miền cấp 2 chung, không xét cấp phát
                        StatusWhois = Utility.StatusWhois.NotFound;
                        return true;
                    }
                }

                if (DomainObjectBase == null)
                    return false;

                DomainObjectBase.DomainName = DomainName;
                DomainObjectBase.WhoisServerName = "NhanHoa";
                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }
    }
}
