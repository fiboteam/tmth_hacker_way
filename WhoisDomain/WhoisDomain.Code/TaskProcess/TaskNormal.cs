﻿using WhoisDomain.Code.Process;
using WhoisDomain.CodeObjects;
using WhoisDomain.Utility;
using WhoisDomain.Objects;
using WhoisDomain.DAO.SqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Threading;
using System.Threading.Tasks;

namespace WhoisDomain.Code.TaskProcess
{
    public class TaskNormal: TaskBase
    {
        int TaskNormal_BeforeExpireDate = int.Parse(Tool.GetParamAppConfig("TaskNormal_BeforeExpireDate"));
        int TaskNormal_AfterExpireDate = int.Parse(Tool.GetParamAppConfig("TaskNormal_AfterExpireDate"));
        long TaskNormal_AmountRecord = long.Parse(Tool.GetParamAppConfig("TaskNormal_AmountRecord"));
        int TaskNormal_Interval = int.Parse(Tool.GetParamAppConfig("TaskNormal_Interval"));
        string TaskNormal_SP = Tool.GetParamAppConfig("TaskNormal_SP");

        List<ProcessBase> ListProcessWhois;
        List<string> TaskNormal_Process_GetByRegistrar;
        Dictionary<string, List<tblDomainVN>> dicDomainOfProcess;
        System.Timers.Timer timerMain = new System.Timers.Timer();
        SqlDomainVNDao _sqlDomainVNDao = new SqlDomainVNDao();

        int WarningErrorWhois = int.Parse(Tool.GetParamAppConfig("WarningErrorWhois"));
        Dictionary<string, int> dicErrorWhois;

        public TaskNormal()
        {
            timerMain.Interval = 1000 * TaskNormal_Interval;
            timerMain.Elapsed += timerMain_Elapsed;
        }

        void LoadProcessWhois()
        {
            try
            {
                ListProcessWhois = new List<ProcessBase>();
                TaskNormal_Process_GetByRegistrar = new List<string>(Tool.GetParamAppConfig("TaskNormal_Process_GetByRegistrar").Split('|'));
                string[] listProcessName = Tool.GetParamAppConfig("TaskNormal_ListProcess").Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
                string TaskNormal_ProcessNamespace = Tool.GetParamAppConfig("TaskNormal_ProcessNamespace");

                foreach (var i in listProcessName)
                {
                    object objProcess = Activator.CreateInstance(Type.GetType(TaskNormal_ProcessNamespace.Replace("@@@", i)));
                    ListProcessWhois.Add((ProcessBase)objProcess);
                }
            }
            catch (Exception)
            {
                ListProcessWhois = null;
            }
        }

        public bool Start()
        {
            try
            {
                if (!HasStop)
                    return false;

                HasStop = false;
                CallStop = false;
                LoadProcessWhois();

                if (ListProcessWhois == null || ListProcessWhois.Count == 0)
                    return false;

                if (TaskNormal_Process_GetByRegistrar == null || TaskNormal_Process_GetByRegistrar.Count == 0)
                    return false;

                if (TaskNormal_Process_GetByRegistrar.Count(p => String.IsNullOrEmpty(p)) != 1)
                    return false;

                dicErrorWhois = new Dictionary<string, int>();
                foreach(var i in ListProcessWhois)
                {
                    dicErrorWhois.Add(i.GetType().Name, 0);
                }

                timerMain.Enabled = true;
                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }

        public bool Stop()
        {
            try
            {
                CallStop = true;
                while (!HasStop && timerMain.Enabled) { }

                HasStop = true;
                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }

        void timerMain_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                if (!FinishCoordinator)
                    return;

                Task.Factory.StartNew(() => {
                    FinishCoordinator = false;

                    Coordinator();
                    CoordinatorWhois();

                    if (CallStop)
                    {
                        timerMain.Enabled = false;
                        HasStop = true;
                    }

                    FinishCoordinator = true;
                });
            }
            catch(Exception)
            {
                FinishCoordinator = true;
            }
        }

        void Coordinator()
        {
            try
            {
                dicDomainOfProcess = new Dictionary<string, List<tblDomainVN>>();
                var listDomainVn = _sqlDomainVNDao.GetDomainVN_WhoisNormal(TaskNormal_AmountRecord, TaskNormal_BeforeExpireDate, TaskNormal_AfterExpireDate, TaskNormal_SP);
                
                if(listDomainVn == null || listDomainVn.Count == 0)
                {
                    return;
                }

                string getByRegistrar = "";
                int indexDefault = -1;
                for (int i = 0; i < ListProcessWhois.Count; i++)
                {
                    if (CallStop)
                        return;

                    if (String.IsNullOrEmpty(TaskNormal_Process_GetByRegistrar[i]))
                    {
                        indexDefault = i;
                        continue;
                    }

                    getByRegistrar = TaskNormal_Process_GetByRegistrar[i].ToLower();
                    List<tblDomainVN> listItems = new List<tblDomainVN>();
                    for (int j = 0; j < listDomainVn.Count; j++)
                    {
                        if (CallStop)
                            return;

                        if (listDomainVn[j].CurrentRegistrar.ToLower().Contains(getByRegistrar))
                        {
                            listItems.Add(listDomainVn[j]);
                            listDomainVn.RemoveAt(j);
                            j--;
                        }

                        if ((j + 1) < 0)
                            break;
                    }

                    dicDomainOfProcess.Add(ListProcessWhois[i].GetType().Name, listItems);
                }

                dicDomainOfProcess.Add(ListProcessWhois[indexDefault].GetType().Name, listDomainVn);
            }
            catch(Exception)
            {

            }
        }

        void CoordinatorWhois()
        {
            try
            {
                List<Task> listTask = new List<Task>();
                foreach(var i in dicDomainOfProcess)
                {
                    listTask.Add(Task.Factory.StartNew(() => {
                        ProcessWhois(ListProcessWhois.FirstOrDefault(p => p.GetType().Name.Equals(i.Key)), i.Value);
                    }));
                }
                Task.WaitAll(listTask.ToArray());
            }
            catch(Exception)
            {

            }
        }

        void ProcessWhois(ProcessBase processBase, List<tblDomainVN> listSource)
        {
            try
            {
                foreach(var i in listSource)
                {
                    if (CallStop)
                        return;

                    var item = processBase.Get(i.DomainName);

                    if(item == null)
                    {
                        if (processBase.StatusWhois != StatusWhois.Registered)
                        {
                            item = new DomainObjectBase()
                            {
                                DomainName = i.DomainName,
                                CurrentRegistrar = "",
                                DNSServer = "",
                                ExpireDate = DateTime.Parse("1900-01-01"),
                                Owner = "",
                                RegistrationDate = DateTime.Parse("1900-01-01"),
                                WhoisServerName = processBase.GetType().Name.Replace("Process", "")
                            };

                            item.ShortExpireDate = int.Parse(item.ExpireDate.ToString("yyyyMMdd"));
                            SetErrorWhois(processBase.GetType().Name, true);
                        }
                        else
                        {
                            SetErrorWhois(processBase.GetType().Name, false);
                            continue;
                        }
                    }
                    else
                    {
                        SetErrorWhois(processBase.GetType().Name, true);
                    }

                    if (i.AssignProperties(item))
                    {
                        if (!_sqlDomainVNDao.UpdateDomainVN_WhoisNormal(i, i.ExtenstionProperties["MAXSHORTLASTDATEWHOIS"].ToString()))
                            ErrorDB++;
                        else
                            ErrorDB = 0;
                    }
                }
            }
            catch(Exception)
            {

            }
        }

        void SetErrorWhois(string processName, bool reset)
        {
            try
            {
                if (reset)
                {
                    dicErrorWhois[processName] = 0;
                }
                else
                {
                    dicErrorWhois[processName]++;

                    if (dicErrorWhois[processName] >= WarningErrorWhois)
                    {
                        dicErrorWhois[processName] = 0;
                    }
                }
            }
            catch(Exception)
            {

            }
        }
    }
}
