﻿using WhoisDomain.CodeObjects;
using WhoisDomain.Code.Process;
using WhoisDomain.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace WhoisDomain.Code.TaskProcess
{
    public class TaskMain
    {
        TaskNormal TaskNormal = new TaskNormal();
        TaskHasExpire TaskHasExpire = new TaskHasExpire();


        public TaskMain()
        {
        }

        public bool Start()
        {
            try
            {
                TaskNormal = new TaskNormal();
                TaskNormal.Start();

                TaskHasExpire = new TaskHasExpire();
                TaskHasExpire.Start();
                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }

        public bool Stop()
        {
            try
            {
                TaskNormal.Stop();
                TaskHasExpire.Stop();

                TaskNormal = null;
                TaskHasExpire = null;

                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }
    }
}
