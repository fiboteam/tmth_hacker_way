﻿using WhoisDomain.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WhoisDomain.Code.TaskProcess
{
    public class TaskBase
    {
        //delegate void WriteLog(string content, bool isMail = false);
        //public event WriteLog WriteLogEvent;

        protected int WarningErrorDB = int.Parse(Tool.GetParamAppConfig("WarningErrorDB"));

        int _errorDB = 0;
        protected int ErrorDB 
        { 
            set 
            { 
                _errorDB = value;
                if(_errorDB >= WarningErrorDB)
                {
                    _errorDB = 0;
                }
            }
            get { return _errorDB; } 
        }


        protected bool FinishCoordinator = true;
        protected bool HasStop = true;
        protected bool CallStop = false;

        public TaskBase()
        {
            
        }

        void TaskBase_WriteLogEvent(string content, bool isMail = false)
        {
            content += "\r\n";
            
        }
    }
}
