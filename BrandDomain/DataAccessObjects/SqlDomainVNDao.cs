﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using BusinessObjects;
using System.Text.RegularExpressions;
using Utility;

namespace DataAccessObjects
{
	public class SqlDomainVNDao
    {
		private string _connectionStr;
		private SqlConnection conn;
		private SqlDataReader rdr = null;

		public SqlDomainVNDao()
		{
			string connectionStringName = ConfigurationManager.AppSettings.Get("ConnectionStringName");
			_connectionStr = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
		}

		public void Open()
		{
			if(conn==null)
			{
				conn = new SqlConnection(_connectionStr);
			}

			conn.Open();
		}

		public void Close()
		{
			conn.Close();
		}

		public ObjectCompareDomainVN GetNameDomainVN(DateTime expireDate)
		{
			ObjectCompareDomainVN objectCompareDomainVN = new ObjectCompareDomainVN();
			string name = "";
			//mẫu cắt chuỗi
			string pattern = @"\.";
			Regex regex = new Regex(pattern);
			Regex regexVN = null ;
			string patternVN=null;
			Regex regexComVN = null; ;
			string patternComVN=null;
			GroupDomainVN group = null ;
			try
			{
				Open();
				// 1. create a command object identifying
				// the stored procedure
				SqlCommand cmd = new SqlCommand(
					"spDomainVN_GetDomainVNWithFilter", conn);

				// 2. set the command object so it knows
				// to execute a stored procedure
				cmd.CommandType = CommandType.StoredProcedure;

				// 3. add parameter to command, which
				// will be passed to the stored procedure
				cmd.Parameters.Add(
					new SqlParameter("@ExpireDate", expireDate));

				// execute the command
				rdr = cmd.ExecuteReader();

				// iterate through results, printing each to console
				while (rdr.Read())
				{
				
					DomainVN domain = new DomainVN();

					name = rdr["DomainName"].ToString().Trim();
					name = name.ToLower();
					domain.DomainName = name;
					name = rdr["Prefix"].ToString().Trim();
					name = name.ToLower();
					domain.Prefix = name;

					if (!string.IsNullOrEmpty(domain.DomainName))
					{
						//Add tất cả domain lấy từ db && không rỗng và list kết quả.
						objectCompareDomainVN.ListDomainVN.Add(domain);

						//Xử lý group

						if (group == null)
						{
							//group = new GroupDomainVN();

							string[] part = regex.Split(domain.DomainName);
							patternVN = @"^" + part[0] + "\\.vn$";
							regexVN = new Regex(patternVN);
							patternComVN = @"^" + part[0] + "\\.com\\.vn$";
							regexComVN = new Regex(patternComVN);
				
							group = NewGroupDomainVn(domain.DomainName,regexVN,regexComVN);
						
						}
						else
						{

							if (regexComVN.IsMatch(domain.DomainName) || regexVN.IsMatch(domain.DomainName))
							{
								group.TotalDomain = EnumDomain.Both;

								objectCompareDomainVN.ListGroupDomainVN.Add(group);

								group = null;
							}
							else
							{
								if (group.TotalDomain != EnumDomain.NotVn)
								{
									objectCompareDomainVN.ListGroupDomainVN.Add(group);

									group = null;
								}

								string[] part = regex.Split(domain.DomainName);
								patternVN = @"^" + part[0] + "\\.vn$";
								regexVN = new Regex(patternVN);
								patternComVN = @"^" + part[0] + "\\.com\\.vn$";
								regexComVN = new Regex(patternComVN);

								group = NewGroupDomainVn(domain.DomainName, regexVN, regexComVN);
							}

						}
					}
					else
					{
						string n = domain.DomainName;
					}
				}

				return objectCompareDomainVN;
			}
			catch
			{
				return objectCompareDomainVN;
			}
			finally
			{
				Close();
			}
			
		}

		public List<GroupDomainVn2> GetGroupDomainVn(DateTime expireDate)
		{
			List<GroupDomainVn2> groups = new List<GroupDomainVn2>();
			try
			{
				Open();
				// 1. create a command object identifying
				// the stored procedure
				SqlCommand cmd = new SqlCommand(
					"spDomainVN_GetDomainVNWithFilter", conn);

				// 2. set the command object so it knows
				// to execute a stored procedure
				cmd.CommandType = CommandType.StoredProcedure;

				// 3. add parameter to command, which
				// will be passed to the stored procedure
				cmd.Parameters.Add(
					new SqlParameter("@ExpireDate", expireDate));

				// execute the command
				rdr = cmd.ExecuteReader();

				// iterate through results, printing each to console
				while (rdr.Read())
				{

					GroupDomainVn2 domain = new GroupDomainVn2();

					string prefix = rdr["Prefix"].ToString().Trim();
					domain.Prefix = prefix.ToLower();

					string domainComvn = rdr["DomainComvn"].ToString().Trim();
					domain.DomainComVn = domainComvn.ToLower();

					string domainVn = rdr["DomainVn"].ToString().Trim();
					domain.DomainVn = domainVn.ToLower();

					int totalDomain = int.Parse(rdr["TotalDomain"].ToString());
					domain.TotalDomain = totalDomain;

					groups.Add(domain);
				}

				return groups;
			}
			catch
			{
				return groups;
			}
			finally
			{
				Close();
			}
			
		}

		private GroupDomainVN NewGroupDomainVn(string domain, Regex regexVN, Regex regexComVN)
		{
			GroupDomainVN group = new GroupDomainVN();

			string pattern = @"\.";
			Regex regex = new Regex(pattern);
			string[] part = regex.Split(domain);

			group.Name = part[0].ToLower();
			group.Name = group.Name.Trim();

			if (regexComVN.IsMatch(domain.ToLower()))
			{
				group.TotalDomain = EnumDomain.ComVn;
			}
			else if (regexVN.IsMatch(domain.ToLower()))
			{
				group.TotalDomain = EnumDomain.Vn;
			}
			else
			{
				group = null;
			}

			return group;
		}

		public List<string> CheckCompareDomain(DateTime datime, string domain)
		{
			List<string> listNames = new List<string>();
			try
			{
				Open();
				// 1. create a command object identifying
				// the stored procedure
				SqlCommand cmd = new SqlCommand(
					"spDomainVN_CompareDomain", conn);

				// 2. set the command object so it knows
				// to execute a stored procedure
				cmd.CommandType = CommandType.StoredProcedure;

				// 3. add parameter to command, which
				// will be passed to the stored procedure
				cmd.Parameters.Add(
					new SqlParameter("@ExpireDate", datime));

				cmd.Parameters.Add(
					new SqlParameter("@Domain", domain));

				// execute the command
				rdr = cmd.ExecuteReader();

				

				// iterate through results, printing each to console
				while (rdr.Read())
				{
					
					string prefix = rdr["DomainName"].ToString().Trim();
					prefix = prefix.ToLower();
					listNames.Add(prefix);
				}

				return listNames;

			}
			catch
			{
				return listNames;
			}
			finally
			{
				Close();
			}
		}
	}
}
