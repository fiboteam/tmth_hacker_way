﻿namespace CompareDomain
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.btnSelectFileDomainInternational = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.txtFileDomainInternational = new System.Windows.Forms.TextBox();
			this.rtxtView = new System.Windows.Forms.RichTextBox();
			this.cbbSheet = new System.Windows.Forms.ComboBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.cbbColumnDomainName = new System.Windows.Forms.ComboBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.ckbExportWhenCompare = new System.Windows.Forms.CheckBox();
			this.txtThreads = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.btnCompare = new System.Windows.Forms.Button();
			this.btnExport = new System.Windows.Forms.Button();
			this.btnClear = new System.Windows.Forms.Button();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.timerProgress = new System.Windows.Forms.Timer(this.components);
			this.btnTestCompare = new System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnSelectFileDomainInternational
			// 
			this.btnSelectFileDomainInternational.Location = new System.Drawing.Point(333, 15);
			this.btnSelectFileDomainInternational.Name = "btnSelectFileDomainInternational";
			this.btnSelectFileDomainInternational.Size = new System.Drawing.Size(75, 23);
			this.btnSelectFileDomainInternational.TabIndex = 1;
			this.btnSelectFileDomainInternational.Text = "Select";
			this.btnSelectFileDomainInternational.UseVisualStyleBackColor = true;
			this.btnSelectFileDomainInternational.Click += new System.EventHandler(this.btnSelectFileDomainInternational_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 20);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(126, 13);
			this.label1.TabIndex = 2;
			this.label1.Text = "Chọn file domain Quốc tế";
			// 
			// txtFileDomainInternational
			// 
			this.txtFileDomainInternational.Location = new System.Drawing.Point(144, 16);
			this.txtFileDomainInternational.Name = "txtFileDomainInternational";
			this.txtFileDomainInternational.Size = new System.Drawing.Size(181, 20);
			this.txtFileDomainInternational.TabIndex = 3;
			// 
			// rtxtView
			// 
			this.rtxtView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.rtxtView.Location = new System.Drawing.Point(1, 157);
			this.rtxtView.Name = "rtxtView";
			this.rtxtView.Size = new System.Drawing.Size(700, 206);
			this.rtxtView.TabIndex = 4;
			this.rtxtView.Text = "";
			// 
			// cbbSheet
			// 
			this.cbbSheet.FormattingEnabled = true;
			this.cbbSheet.Location = new System.Drawing.Point(144, 42);
			this.cbbSheet.Name = "cbbSheet";
			this.cbbSheet.Size = new System.Drawing.Size(121, 21);
			this.cbbSheet.TabIndex = 6;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(12, 46);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(63, 13);
			this.label3.TabIndex = 8;
			this.label3.Text = "Chọn Sheet";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(12, 73);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(93, 13);
			this.label4.TabIndex = 10;
			this.label4.Text = "Cột chứa tên miền";
			// 
			// cbbColumnDomainName
			// 
			this.cbbColumnDomainName.FormattingEnabled = true;
			this.cbbColumnDomainName.Location = new System.Drawing.Point(144, 69);
			this.cbbColumnDomainName.Name = "cbbColumnDomainName";
			this.cbbColumnDomainName.Size = new System.Drawing.Size(121, 21);
			this.cbbColumnDomainName.TabIndex = 9;
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.ckbExportWhenCompare);
			this.groupBox1.Controls.Add(this.txtThreads);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Location = new System.Drawing.Point(414, 2);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(287, 108);
			this.groupBox1.TabIndex = 11;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Config";
			// 
			// ckbExportWhenCompare
			// 
			this.ckbExportWhenCompare.AutoSize = true;
			this.ckbExportWhenCompare.Checked = true;
			this.ckbExportWhenCompare.CheckState = System.Windows.Forms.CheckState.Checked;
			this.ckbExportWhenCompare.Location = new System.Drawing.Point(10, 85);
			this.ckbExportWhenCompare.Name = "ckbExportWhenCompare";
			this.ckbExportWhenCompare.Size = new System.Drawing.Size(179, 17);
			this.ckbExportWhenCompare.TabIndex = 2;
			this.ckbExportWhenCompare.Text = "Export sau khi hoàn tất compare";
			this.ckbExportWhenCompare.UseVisualStyleBackColor = true;
			// 
			// txtThreads
			// 
			this.txtThreads.Location = new System.Drawing.Point(64, 16);
			this.txtThreads.Name = "txtThreads";
			this.txtThreads.ReadOnly = true;
			this.txtThreads.Size = new System.Drawing.Size(100, 20);
			this.txtThreads.TabIndex = 1;
			this.txtThreads.Text = "2";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(7, 20);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(49, 13);
			this.label5.TabIndex = 0;
			this.label5.Text = "Threads:";
			// 
			// btnCompare
			// 
			this.btnCompare.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCompare.Location = new System.Drawing.Point(615, 116);
			this.btnCompare.Name = "btnCompare";
			this.btnCompare.Size = new System.Drawing.Size(75, 23);
			this.btnCompare.TabIndex = 12;
			this.btnCompare.Text = "Compare";
			this.btnCompare.UseVisualStyleBackColor = true;
			this.btnCompare.Click += new System.EventHandler(this.btnCompare_Click);
			// 
			// btnExport
			// 
			this.btnExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnExport.Location = new System.Drawing.Point(534, 116);
			this.btnExport.Name = "btnExport";
			this.btnExport.Size = new System.Drawing.Size(75, 23);
			this.btnExport.TabIndex = 13;
			this.btnExport.Text = "Export";
			this.btnExport.UseVisualStyleBackColor = true;
			this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
			// 
			// btnClear
			// 
			this.btnClear.Location = new System.Drawing.Point(1, 116);
			this.btnClear.Name = "btnClear";
			this.btnClear.Size = new System.Drawing.Size(75, 23);
			this.btnClear.TabIndex = 14;
			this.btnClear.Text = "Clear";
			this.btnClear.UseVisualStyleBackColor = true;
			this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
			// 
			// progressBar1
			// 
			this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.progressBar1.Location = new System.Drawing.Point(1, 145);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(700, 10);
			this.progressBar1.TabIndex = 17;
			// 
			// timerProgress
			// 
			this.timerProgress.Tick += new System.EventHandler(this.timerProgress_Tick);
			// 
			// btnTestCompare
			// 
			this.btnTestCompare.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnTestCompare.Location = new System.Drawing.Point(424, 116);
			this.btnTestCompare.Name = "btnTestCompare";
			this.btnTestCompare.Size = new System.Drawing.Size(104, 23);
			this.btnTestCompare.TabIndex = 18;
			this.btnTestCompare.Text = "Test compare";
			this.btnTestCompare.UseVisualStyleBackColor = true;
			this.btnTestCompare.Click += new System.EventHandler(this.btnTestCompare_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(702, 365);
			this.Controls.Add(this.btnTestCompare);
			this.Controls.Add(this.progressBar1);
			this.Controls.Add(this.btnClear);
			this.Controls.Add(this.btnExport);
			this.Controls.Add(this.btnCompare);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.cbbColumnDomainName);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.cbbSheet);
			this.Controls.Add(this.rtxtView);
			this.Controls.Add(this.txtFileDomainInternational);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.btnSelectFileDomainInternational);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "Form1";
			this.Text = "Compare BrandDomain";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnSelectFileDomainInternational;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtFileDomainInternational;
		private System.Windows.Forms.RichTextBox rtxtView;
		private System.Windows.Forms.ComboBox cbbSheet;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.ComboBox cbbColumnDomainName;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.CheckBox ckbExportWhenCompare;
		private System.Windows.Forms.TextBox txtThreads;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Button btnCompare;
		private System.Windows.Forms.Button btnExport;
		private System.Windows.Forms.Button btnClear;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Timer timerProgress;
		private System.Windows.Forms.Button btnTestCompare;
	}
}

