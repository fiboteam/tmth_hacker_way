﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BrandDomainService;
using System.Threading.Tasks;
using BusinessObjects;
using System.Threading;
using System.Collections.Concurrent;
using System.Text.RegularExpressions;
using Utility;
using NPOI.XSSF.UserModel;
using System.Diagnostics;
using System.IO;

namespace CompareDomain
{
	public partial class Form1 : Form
	{
		private Task taskCompareDomain;
		private Task taskTestCompareDomain;
		private BrandDomainService01 _brandDomainService = new BrandDomainService01();


		public delegate void WriteRichTextBoxDelegate(string str, RichTextBox rtb);
		public WriteRichTextBoxDelegate mWriteRichTextBox = null;

		public delegate void WriteLabelDelegate(float str, Label rtb);
		public WriteLabelDelegate mWriteLabel = null;

		public delegate void RunBackgroundWorkerDelegate(bool str, BackgroundWorker rtb);
		public RunBackgroundWorkerDelegate mRunBackgroundWorkerDelegate = null;

		public delegate void SetValueProgressDelegate(int value, ProgressBar progressBar);
		public SetValueProgressDelegate mSetValueProgressbar = null;

		public delegate void SetValueMaximumProgressDelegate(int value, ProgressBar progressBar);
		public SetValueMaximumProgressDelegate mSetValueMaximumProgress = null;

		private List<Domain> _resultCompare = new List<Domain>();
		private List<Domain> _resultCheckCompare = new List<Domain>();
		private List<DomainVN> domainVN = new List<DomainVN>();
		//private List<GroupDomainVN> groupDomainVN = new List<GroupDomainVN>();
		private int CountProgress = 0;
		private int maxProgress = 0;
		private string parrtern = "";
		private ObjectCompareDomainVN ObCompareDomainVN;
		

		private object lockMain = new object();

		private BackgroundWorker _bgwProcessBar = new BackgroundWorker();

		public Form1()
		{
			InitializeComponent();
			InitializeElements();
		}

		private void InitializeElements()
		{
			var listSheet=GFunction.ConverEnumToListItem<ExcelSheetAt>();
			cbbSheet.DataSource = listSheet;
			cbbSheet.DisplayMember = "Name";
			cbbSheet.ValueMember = "Value";

			var listCoumn = GFunction.ConverEnumToListItem<ExcelColunmAt>();
			cbbColumnDomainName.DataSource = listCoumn;
			cbbColumnDomainName.DisplayMember = "Name";
			cbbColumnDomainName.ValueMember = "Value";

			_bgwProcessBar.WorkerReportsProgress = true;
			_bgwProcessBar.WorkerSupportsCancellation = true;

			_bgwProcessBar.DoWork += bgwProcessBar_DoWork;
			_bgwProcessBar.ProgressChanged += new ProgressChangedEventHandler(bgwProcessBar_ProgressChanged);
			_bgwProcessBar.RunWorkerCompleted += bgwProcessBar_RunWorkerCompleted;
			progressBar1.Maximum = 0;
			progressBar1.Minimum = 0;
		}

		

		private void Form1_Load(object sender, EventArgs e)
		{
			//txtThreads.Text = (Environment.ProcessorCount).ToString();
			txtThreads.Text = "2";
			mWriteRichTextBox += WriteRichTextBox;
			mWriteLabel += WriteLabel;
			mRunBackgroundWorkerDelegate += RunBackgroundWorker;
			mSetValueProgressbar += SetValueProgressbar;
			mSetValueMaximumProgress += SetValueMaximumProgress;

			timerProgress.Interval = 100;
		}

		private void bgwProcessBar_ProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			//progressBar1.Value = e.ProgressPercentage;
			this.Invoke(mSetValueProgressbar, new object[] { e.ProgressPercentage, progressBar1 });
		}

		private void bgwProcessBar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			
		}

		private void bgwProcessBar_DoWork(object sender, DoWorkEventArgs e)
		{
			_bgwProcessBar.ReportProgress(10);
		}

		

		private void SetValueMaximumProgress(int value, ProgressBar progressBar)
		{
			progressBar.Maximum = value;
		}

		private void SetValueProgressbar(int value, ProgressBar progressBar)
		{
			try
			{
				if (value == 0)
				{
					progressBar.Value = 0;
				}
				else
				{
					progressBar.Value += value;
				}
			}
			catch(Exception ex)
			{
			}
		}

		private void RunBackgroundWorker(bool str, BackgroundWorker rtb)
		{
			if(!rtb.IsBusy && str)
			{
				rtb.RunWorkerAsync();
			}
		}

		private void WriteLabel(float str, Label lbl)
		{
			lbl.Text = str.ToString();
		}

		private void WriteRichTextBox(string str, RichTextBox rtb)
		{
			try
			{
				rtb.AppendText(str + "\n");
			}
			catch { }
		}

		private void btnSelectFileDomainInternational_Click(object sender, EventArgs e)
		{
			OpenFileDialog dialog = new OpenFileDialog();
			dialog.Filter = "file xlsx (*.xlsx)|*.xlsx";
			dialog.Title = "Chọn file tên miền quốc tế";

			dialog.CheckFileExists = true;
			dialog.CheckPathExists = true;

			if(dialog.ShowDialog()==DialogResult.OK)
			{
				txtFileDomainInternational.Text = dialog.FileName;
			}

			if(string.IsNullOrEmpty( txtFileDomainInternational.Text))
			{
				this.BeginInvoke(mWriteRichTextBox, new object[] { "[Warning] File không tồn tại ", rtxtView });
				return;
			}

			
			
		}

		private void ProcessCompareDomainInternationalAndVn()
		{
			string filename = txtFileDomainInternational.Text;
			int sheet = ((SelectItem)cbbSheet.SelectedItem).Value;
			int column = ((SelectItem)cbbColumnDomainName.SelectedItem).Value;
			int threads = 0;
			int.TryParse(txtThreads.Text, out threads);
			bool allowExport = ckbExportWhenCompare.Checked;

			if (taskCompareDomain == null)
			{
				CountProgress = 0;
				progressBar1.Value = 0;
				progressBar1.Maximum=0;
				taskCompareDomain = Task.Factory.StartNew(() =>
				{
					CompareDomainInternationalAndVn(filename, sheet, column, threads, allowExport);
				});
			}
			else
			{
				if (!taskCompareDomain.IsCompleted)
				{
					this.BeginInvoke(mWriteRichTextBox, new object[] { "[Warning] Đang compare ", rtxtView });
				}
				else
				{
					CountProgress = 0;
					progressBar1.Value = 0;
					progressBar1.Maximum = 0;
					taskCompareDomain = Task.Factory.StartNew(() =>
					{
						CompareDomainInternationalAndVn(filename, sheet, column, threads, allowExport);
					});
				}
			}
		}

		private void CompareDomainInternationalAndVn(string filename,int sheet,int columnName,int threads,bool allowExport)
		{
			try
			{
				Stopwatch totalTile = new Stopwatch();
				Stopwatch watch = new Stopwatch();
				this.Invoke(mSetValueMaximumProgress, new object[] { 30, progressBar1 });

				this.BeginInvoke(mWriteRichTextBox, new object[] { "\n----------Compare Domain.", rtxtView });
				totalTile.Start();
				this.BeginInvoke(mWriteRichTextBox, new object[] { "\n[Begin] Lấy domain quốc tế từ file", rtxtView });
				watch.Start();
				///Domain quoc tế
				List<Domain> domainsImportFromFile = new List<Domain>();
				List<GroupDomainVn2> _groupDomainVn=new List<GroupDomainVn2>();
				//nhóm cac ky tu trong domain quoc te
				List<RangeName> listRangeName = new List<RangeName>();

				domainsImportFromFile = _brandDomainService.GetDomainInternationalFromExcel(filename, sheet, columnName).ToList();
				_bgwProcessBar.RunWorkerAsync();
				watch.Stop();
				this.BeginInvoke(mWriteRichTextBox, new object[] { "[Elapsed] " + watch.Elapsed.TotalSeconds + " seconds", rtxtView });
				this.BeginInvoke(mWriteRichTextBox, new object[] { "[Total] Số lượng domain quốc tế: " + domainsImportFromFile.Count, rtxtView });
				this.BeginInvoke(mWriteRichTextBox, new object[] { "[End] Lấy domain quốc tế từ file", rtxtView });

				if (domainsImportFromFile.Count>0)
				{
					if (domainVN.Count <= 0)
					{
						this.BeginInvoke(mWriteRichTextBox, new object[] { "\n[Bengin] Lấy danh sách domain VN", rtxtView });
						watch.Restart();
			
						_groupDomainVn = _brandDomainService.GetGroupDomainVn(DateTime.Now);
						_bgwProcessBar.RunWorkerAsync();
						watch.Stop();
						this.BeginInvoke(mWriteRichTextBox, new object[] { "[Elapsed] " + watch.Elapsed.TotalSeconds + " seconds", rtxtView });
						this.BeginInvoke(mWriteRichTextBox, new object[] { "[Total] Số lượng domain VN: " + _groupDomainVn.Count, rtxtView });
						this.BeginInvoke(mWriteRichTextBox, new object[] { "[End] Lấy danh sách domain VN", rtxtView });
						
					}

					if (_groupDomainVn.Count > 0)
					{
						this.BeginInvoke(mWriteRichTextBox, new object[] { "\n[Begin] Chia nhóm domain quốc tế", rtxtView });
						watch.Restart();
						///tạo nhóm các domain quốc tế
						for (int i = threads; i > 0; i--)
						{
							PrepareFillterThird(domainsImportFromFile, listRangeName, i, _groupDomainVn);
						}
						watch.Stop();
						_bgwProcessBar.RunWorkerAsync();
						this.BeginInvoke(mWriteRichTextBox, new object[] { "[Elapsed] " + watch.Elapsed.TotalSeconds + " seconds", rtxtView });
						this.BeginInvoke(mWriteRichTextBox, new object[] { "[Total] Số lượng nhóm domain quốc tế: " + listRangeName.Count, rtxtView });
						this.BeginInvoke(mWriteRichTextBox, new object[] { "[End] Chia nhóm domain quốc tế", rtxtView });


						this.BeginInvoke(mWriteRichTextBox, new object[] { "\n[Begin] Compare domain quốc tế và domain VN", rtxtView });
						//this.BeginInvoke(mWriteRichTextBox, new object[] { "[Begin] Chọn domain VN", rtxtView });
						//watch.Restart();
						///Xác dinh groupdomain vn sẽ chạy từ khoản nào đến khoản nào
						///
						//OptimizeGroupDomainVN(_groupDomainVn, listRangeName, threads);

						//watch.Stop();
						//this.BeginInvoke(mWriteRichTextBox, new object[] { "[Elapsed] " + watch.Elapsed.TotalSeconds + " Seconds", rtxtView });
						//this.BeginInvoke(mWriteRichTextBox, new object[] { "[End] Chọn domain VN", rtxtView });

						this.Invoke(mSetValueProgressbar, new object[] { 0, progressBar1 });
						this.Invoke(mSetValueMaximumProgress, new object[] { (_groupDomainVn.Count+1)*10, progressBar1 });
						this.BeginInvoke(mWriteRichTextBox, new object[] { "\n[Begin] Compare tên miền", rtxtView });
						watch.Restart();
						this.BeginInvoke(mWriteRichTextBox, new object[] { "Processing...", rtxtView });
						
						ProcessCompare(domainsImportFromFile.ToList(), domainVN.ToList(), threads, listRangeName, _groupDomainVn);
						//_resultCompare = domainsImportFromFile.ToList();

						watch.Stop();
						this.BeginInvoke(mWriteRichTextBox, new object[] { "[Elapsed] " + watch.Elapsed.TotalMinutes + " minutes", rtxtView });
						this.BeginInvoke(mWriteRichTextBox, new object[] { "[End] Compare tên miền. Total Record: " + domainsImportFromFile.Count, rtxtView });
						watch.Reset();
					}
				}

				if (domainsImportFromFile.Count > 0 && allowExport)
				{
					_resultCompare.Clear();
					_resultCompare = domainsImportFromFile;
					ProcessExportResultCompore();
				}
				totalTile.Stop();
				this.BeginInvoke(mWriteRichTextBox, new object[] { "\n[Elapsed] Tổng thời gian: " + totalTile.Elapsed.TotalMinutes + " minutes", rtxtView });
			}
			catch(Exception ex)
			{
				this.BeginInvoke(mWriteRichTextBox, new object[] { "[Error] "+ex.ToString(), rtxtView });
				_bgwProcessBar.CancelAsync();
			}
		}

		private List<GroupDomainVn2> OptimizeGroupDomainVN(List<GroupDomainVn2> groupDomainVN, List<RangeName> listRangeName, int threads)
		{
			var locker = new object();
			//List<GroupDomainVn2> list = new List<GroupDomainVn2>();
			//for (int j = 0; j < groupDomainVN.Count;j++ )
			//{ 

			Parallel.For(0, groupDomainVN.Count, j =>
			{
				//GroupDomainVn2 temp = new GroupDomainVn2(groupDomainVN[j]);

				string firstChar = "";

				for (int i = threads; i >= 1; i--)
				{
					if (groupDomainVN[j].Prefix.Length >= i)
					{
						firstChar = groupDomainVN[j].Prefix.Substring(0, i);
						break;
					}
				}

				RangeName range = FindRangeBy(firstChar, listRangeName);
					
				if (range != null)
				{
					//temp.HaveRange = true;
					//temp.Begin = range.Begin;
					//temp.End = range.End;
					groupDomainVN[j].HaveRange = true;
					groupDomainVN[j].Begin = range.Begin;
					groupDomainVN[j].End = range.End;

				}
				else
				{
					//temp.HaveRange = false;
					groupDomainVN[j].HaveRange = false;
				}
				//lock(locker)
				//{
				//	list.Add(temp);
				//}
			});
			//}
			return groupDomainVN;
		}
		/// <summary>
		/// Tạo danh sách nhóm các domain QT
		/// </summary>
		/// <param name="domainsvn">domain vn</param>
		/// <param name="list">list nhóm domain QT</param>
		/// <param name="level">số lượng ký tự sẽ cắt</param>
		/// <param name="groupDomainVn" type="ref">nhóm domain vn</param>
		private void PrepareFillterThird(List<Domain> domainsvn, List<RangeName> list,int level,List<GroupDomainVn2> groupDomainVn)
		{
			try
			{
				string name = "";
				string temp = "";
				int begin = 0;
				int end = 0;
				//duyệt danh sách domain QT
				for (int i = 0; i < domainsvn.Count; i++)
				{
					//nếu không null và có thể cắt chuỗi
					if (!string.IsNullOrEmpty(domainsvn[i].DomainName) && domainsvn[i].DomainName.Length >= level)
					{
						//cắt chuỗi
						temp = domainsvn[i].DomainName.Substring(0, level);
						//chưa có thì đặt là gốc
						if (name == "")
						{
							name = temp;
							begin = end = i;
						}
							//có rồi thì so sánh
						else
						{
							//nếu là ky tự khác thì lưu gốc vào list nhóm domain QT
							if (name != temp)
							{
								RangeName r = new RangeName();
								r.Name = name;
								r.Begin = begin;
								r.End = end;

								//var count=(from g in list where g.Name == r.Name select g).ToList();
								//if (count.Count>0)
								//{
								//	int asdas = 0;
								//}

								list.Add(r);

								
								(from gr in groupDomainVn
									where gr.Prefix.Length>=level && gr.Prefix.Substring(0,level)==r.Name && gr.HaveRange==false
								 select gr).ToList().ForEach(gr => { gr.HaveRange = true; gr.Begin = r.Begin; gr.End = r.End; });

								//set gốc mới
								name = temp;
								begin = i;
								end = i;

								//nếu là phần từ cuối thì add gốc vào list nhóm domain QT
								if (i == domainsvn.Count - 1)
								{
									r = new RangeName();
									r.Name = name;
									r.Begin = begin;
									r.End = end;

									//var count2 = (from g in list where g.Name == r.Name select g).ToList();
									//if (count2.Count > 0)
									//{
									//	int asdas = 0;
									//}

									list.Add(r);

									groupDomainVn.Where(gr => gr.Prefix.Length >= level && gr.Prefix.Substring(0, level) == r.Name && gr.HaveRange==false).Select(gr => gr).ToList().ForEach(gr => { gr.Begin = r.Begin; gr.End = r.End; gr.HaveRange = true; });
								}
							}
								//nếu bằng thì set lại giá trị end của gốc
							else
							{
								end = i;

								//nếu là phần từ cuối thì add gốc vào list nhóm domain QT
								if (i == domainsvn.Count - 1)
								{
									RangeName r = new RangeName();
									r.Name = name;
									r.Begin = begin;
									r.End = end;

									//var count = (from g in list where g.Name == r.Name select g).ToList();
									//if (count.Count > 0)
									//{
									//	int asdas = 0;
									//}
									list.Add(r);

									groupDomainVn.Where(gr => gr.Prefix.Length >= level && gr.Prefix.Substring(0, level) == r.Name && gr.HaveRange == false).Select(gr => gr).ToList().ForEach(gr => { gr.Begin = r.Begin; gr.End = r.End; gr.HaveRange = true; });
								}
							}
						}
					}
				}

				//sắp sếp theo tên
				list = list.OrderBy(e => e.Name).ToList();
			}
			catch
			{
			}
		}
		/// <summary>
		/// Xử lý compare
		/// </summary>
		/// <param name="domainsInternation">danh sach domain quốc té</param>
		/// <param name="domainsVN">danh sach domain vn</param>
		/// <param name="threads">Hiện tại không dùng đến</param>
		/// <param name="rangeName">nhóm các domain quốc tế</param>
		/// <param name="groupDomainVN">nhóm các domain vn</param>
		/// <returns></returns>
		private void ProcessCompare(List<Domain> domainsInternation, List<DomainVN> domainsVN, int threads, List<RangeName> rangeName,List<GroupDomainVn2> groupDomainVN)
		{
			//List<Domain> domains = new List<Domain>();
			try
			{
				ParallelOptions _options = new ParallelOptions();

				//get new MaxDegreeOfParallelism
				_options.MaxDegreeOfParallelism = threads;

				// Use ConcurrentQueue to enable safe enqueueing from multiple threads. 
				var exceptions = new ConcurrentQueue<Exception>();

				var locker = new Object();
				var lockerDelete = new Object();
				List<Task> listTask = new List<Task>();
				//List<DomainVN> listDomainVN = domainsVN;
				int countProcessed = 0;

				#region foreach v2.1
				//Parallel.For(0, groupDomainVN.Count, i =>
				//{
				Parallel.ForEach(groupDomainVN, _domainVN =>
				{
					try
					{
						this.BeginInvoke((Action)delegate { progressBar1.Value += 10; });
						//Stopwatch sw = new Stopwatch();
						//sw.Start();

						string pattern = @"^" + _domainVN.Prefix + "\\.";
						Regex myRegex = new Regex(pattern);
								

						if (_domainVN.HaveRange)
						{
							//try
							//{

							//(from domain in domainsInternation where myRegex.IsMatch(domain.DomainName) select domain).ToList().ForEach(domain =>
							//{
							//	switch (_domainVN.TotalDomain)
							//	{
							//		case 2:
							//			domain.DomainDotVn = "x";
							//			domain.DomainDotComDotVn = "x";
							//			break;
							//		case 0:
							//			domain.DomainDotVn = "x";
							//			break;
							//		case 1:
							//			domain.DomainDotComDotVn = "x";
							//			break;
							//	}
							//});

							int foundIndex = -1;

							var loopresult = Parallel.For(_domainVN.Begin, _domainVN.End + 1, (int j, ParallelLoopState loop) =>
							{
								var result = CheckDomain(myRegex, domainsInternation[j]);
								if (result)
								{
									switch (_domainVN.TotalDomain)
									{
										case 2:
											domainsInternation[j].DomainDotVn = "x";
											domainsInternation[j].DomainDotComDotVn = "x";
											break;
										case 0:
											domainsInternation[j].DomainDotVn = "x";
											break;
										case 1:
											domainsInternation[j].DomainDotComDotVn = "x";
											break;
									}
									foundIndex = j;
									loop.Break();
									return;
								}
								else
								{

								}
							});

							// Tim phần tử tiếp theo, nếu khác thì break
							for (int i = foundIndex+1; i < _domainVN.End + 1; i++)
							{
								var result = CheckDomain(myRegex, domainsInternation[i]);
								if (result)
								{
									switch (_domainVN.TotalDomain)
									{
										case 2:
											domainsInternation[i].DomainDotVn = "x";
											domainsInternation[i].DomainDotComDotVn = "x";
											break;
										case 0:
											domainsInternation[i].DomainDotVn = "x";
											break;
										case 1:
											domainsInternation[i].DomainDotComDotVn = "x";
											break;
									}
								}
								else
								{
									break;
								}
							}
							// Tim phần tử phía trước, nếu khác thì break
							for (int i = foundIndex - 1; i > _domainVN.Begin -1; i--)
							{
								var result = CheckDomain(myRegex, domainsInternation[i]);
								if (result)
								{
									switch (_domainVN.TotalDomain)
									{
										case 2:
											domainsInternation[i].DomainDotVn = "x";
											domainsInternation[i].DomainDotComDotVn = "x";
											break;
										case 0:
											domainsInternation[i].DomainDotVn = "x";
											break;
										case 1:
											domainsInternation[i].DomainDotComDotVn = "x";
											break;
									}
								}
								else
								{
									break;
								}
							}
					
						}
						else
						{
					
						}

						//sw.Stop();
						//this.BeginInvoke(mWriteRichTextBox, new object[] { "[Elapsed] " + sw.Elapsed.TotalMilliseconds, rtxtView });
					}
					catch (Exception ex)
					{
						exceptions.Enqueue(ex);
					}
				});

				#endregion

			}
			catch
			{
			}
		}

		private RangeName FindRangeBy(string firstChar, List<RangeName> rangeName)
		{
			RangeName found=null;
			Parallel.For(0, rangeName.Count, (int j, ParallelLoopState loop) =>
			{
				if (rangeName[j].Name == firstChar)
				{
					found = rangeName[j];
					loop.Break();
					return;
				}
			});
			return found;
		}

		private bool CheckDomain(Regex regex,Domain domain)
		{
			if (regex.IsMatch(domain.DomainName.ToLower()))
				return true;
			return false;
		}

		private void btnCompare_Click(object sender, EventArgs e)
		{
			ProcessCompareDomainInternationalAndVn();
		}

		private void btnExport_Click(object sender, EventArgs e)
		{
			Task.Factory.StartNew(() => { ProcessExportResultCompore(); });
		}

		private void ProcessExportResultCompore()
		{
			try
			{
				Stopwatch watch = new Stopwatch();
				watch.Start();
				this.BeginInvoke(mWriteRichTextBox, new object[] { "\n----------[Begin] Export", rtxtView });

				//DataTable dt = _brandDomainService.ConvertListToDataTable(_resultCompare);

				if (_resultCompare.Count <= 0)
				{
					this.BeginInvoke(mWriteRichTextBox, new object[] { "[Warning] Không có domain để export", rtxtView });
				}
				else
				{
					this.BeginInvoke(mWriteRichTextBox, new object[] { "Đang thực hiện export...", rtxtView });

					string filename = string.Format("ResultCompare_{0}.xlsx", DateTime.Now.ToString("ddMMyyyyhhmmss"));
					if (!Directory.Exists(System.Environment.CurrentDirectory + "\\UserFile"))
					{
						Directory.CreateDirectory(System.Environment.CurrentDirectory + "\\UserFile");
					}

					string path = System.Environment.CurrentDirectory + "\\UserFile\\" + filename;

					if (CreateExcelFile.CreateExcelDocument<Domain>(_resultCompare, path))
					{
						this.BeginInvoke(mWriteRichTextBox, new object[] { "[Success] " + filename, rtxtView });


						string windir = Environment.GetEnvironmentVariable("windir");
						if (string.IsNullOrEmpty(windir.Trim()))
						{
							windir = "C:\\Windows\\";
						}
						if (!windir.EndsWith("\\"))
						{
							windir += "\\";
						}

						FileInfo fileToLocate = null;
						fileToLocate = new FileInfo(path);

						ProcessStartInfo pi = new ProcessStartInfo(windir + "explorer.exe");
						pi.Arguments = "/select, \"" + fileToLocate.FullName + "\"";
						pi.WindowStyle = ProcessWindowStyle.Normal;
						pi.WorkingDirectory = windir;

						//Start Process
						Process.Start(pi);

						//MessageBox.Show("Export Success");


					}
					else
					{

						this.BeginInvoke(mWriteRichTextBox, new object[] { "[Error] Export error: \n", rtxtView });
					}

					//string name=string.Format("ResultCompare_{0}.xlsx",DateTime.Now.ToString("ddMMyyyyhhmmss"));
					//string error = "";

					//if (_brandDomainService.ExportResultCompareDomain(_resultCompare, name,ref error))
					//{
					//	this.BeginInvoke(mWriteRichTextBox, new object[] { "[Success] " + name, rtxtView });
					//}
					//else
					//{

					//	this.BeginInvoke(mWriteRichTextBox, new object[] { "[Error] Export error: \n" + error, rtxtView });
					//}
				}
				watch.Stop();
				this.BeginInvoke(mWriteRichTextBox, new object[] { "[Time] " + watch.Elapsed.TotalMinutes + " minutes", rtxtView });
				this.BeginInvoke(mWriteRichTextBox, new object[] { "[End] Export", rtxtView });
			}
			catch (Exception ex)
			{
				this.BeginInvoke(mWriteRichTextBox, new object[] { "[Error] Export error: \n" + ex.ToString(), rtxtView });
			}
		}

		private void btnClear_Click(object sender, EventArgs e)
		{
			rtxtView.Clear();
		}

		private void timerProgress_Tick(object sender, EventArgs e)
		{

		}

		private void btnTestCompare_Click(object sender, EventArgs e)
		{
			string filename = txtFileDomainInternational.Text;
			int sheet = ((SelectItem)cbbSheet.SelectedItem).Value;
			int column = ((SelectItem)cbbColumnDomainName.SelectedItem).Value;
			int threads = 0;
			int.TryParse(txtThreads.Text, out threads);
			bool allowExport = ckbExportWhenCompare.Checked;

			if (taskTestCompareDomain == null)
			{
				CountProgress = 0;
				progressBar1.Value = 0;
				progressBar1.Maximum = 0;
				taskTestCompareDomain = Task.Factory.StartNew(() =>
				{
					TestCompareDomain(filename, sheet, column, threads, allowExport);
				});
			}
			else
			{
				if (!taskTestCompareDomain.IsCompleted)
				{
					this.BeginInvoke(mWriteRichTextBox, new object[] { "[Warning] Đang Test compare ", rtxtView });
				}
				else
				{
					CountProgress = 0;
					progressBar1.Value = 0;
					progressBar1.Maximum = 0;
					taskTestCompareDomain = Task.Factory.StartNew(() =>
					{
						TestCompareDomain(filename, sheet, column, threads, allowExport);
					});
				}
			}
		}

		private void TestCompareDomain(string filename, int sheet, int column, int threads, bool allowExport)
		{
			try
			{
				ParallelOptions _options = new ParallelOptions();
				Stopwatch totalTile = new Stopwatch();
				Stopwatch watch = new Stopwatch();
				this.Invoke(mSetValueMaximumProgress, new object[] { 30, progressBar1 });

				this.BeginInvoke(mWriteRichTextBox, new object[] { "\n----------Test Compare Domain.", rtxtView });
				totalTile.Start();
				this.BeginInvoke(mWriteRichTextBox, new object[] { "\n[Begin] Lấy domain quốc tế từ file", rtxtView });
				watch.Start();
				///Domain quoc tế
				List<Domain> domainsImportFromFile = new List<Domain>();
				List<GroupDomainVn2> _groupDomainVn = new List<GroupDomainVn2>();
				//nhóm cac ky tu trong domain quoc te
				List<RangeName> listRangeName = new List<RangeName>();

				domainsImportFromFile = _brandDomainService.GetDomainInternationalFromExcel(filename, sheet, column).ToList();
				watch.Stop();
				this.BeginInvoke(mWriteRichTextBox, new object[] { "[Elapsed] " + watch.Elapsed.TotalSeconds + " seconds", rtxtView });
				this.BeginInvoke(mWriteRichTextBox, new object[] { "[Total] Số lượng domain quốc tế: " + domainsImportFromFile.Count, rtxtView });
				this.BeginInvoke(mWriteRichTextBox, new object[] { "[End] Lấy domain quốc tế từ file", rtxtView });

				

				object locker = new object();
				List<Domain> resultCheckCompare = new List<Domain>();

				if (domainsImportFromFile.Count > 0)
				{
					
					this.BeginInvoke(mWriteRichTextBox, new object[] { "\n[Bengin] Lấy danh sách domain VN", rtxtView });
					watch.Restart();

					_groupDomainVn = _brandDomainService.GetGroupDomainVn(DateTime.Now);

					watch.Stop();
					this.BeginInvoke(mWriteRichTextBox, new object[] { "[Elapsed] " + watch.Elapsed.TotalSeconds + " seconds", rtxtView });
					this.BeginInvoke(mWriteRichTextBox, new object[] { "[Total] Số lượng domain VN: " + _groupDomainVn.Count, rtxtView });
					this.BeginInvoke(mWriteRichTextBox, new object[] { "[End] Lấy danh sách domain VN", rtxtView });

					if (_groupDomainVn.Count>0)
					{
						this.Invoke(mSetValueProgressbar, new object[] { 0, progressBar1 });
						this.Invoke(mSetValueMaximumProgress, new object[] { (_groupDomainVn.Count + 1) * 10, progressBar1 });
						this.BeginInvoke(mWriteRichTextBox, new object[] { "\n[Begin] Check Compare Domain", rtxtView });
						watch.Start();

						Parallel.For(0, _groupDomainVn.Count, i => {

							Domain d = new Domain();

							string pattern = @"^" + _groupDomainVn[i].Prefix + "\\.";
							Regex myRegex = new Regex(pattern);

							domainsImportFromFile.Where(domain => myRegex.IsMatch(domain.DomainName)).Select(domain => domain).ToList().ForEach(domain =>
							{
								switch (_groupDomainVn[i].TotalDomain)
								{
									case 2:
										domain.DomainDotVn = "x";
										domain.DomainDotComDotVn = "x";
										break;
									case 0:
										domain.DomainDotVn = "x";
										break;
									case 1:
										domain.DomainDotComDotVn = "x";
										break;
								}
							});

							this.BeginInvoke((Action)delegate { progressBar1.Value += 10; });


						});
					}

					//_options.MaxDegreeOfParallelism = 50;
					//Parallel.For(0, domainsImportFromFile.Count, i =>
					//{
					////for (int i = 0; i < domainsImportFromFile.Count; i++)
					////{
					//	Domain d=new Domain();
					//	d.DomainName=domainsImportFromFile[i].DomainName;
					//	int result = _brandDomainService.CheckCompareDomain(DateTime.Now, domainsImportFromFile[i].DomainName);
					//	lock (locker)
					//	{
					//		switch (result)
					//		{
					//			case -2:
					//				d.DomainDotComDotVn = "Dữ liệu không hợp lệ";
					//				break;
					//			case 2:
					//				d.DomainDotComDotVn = "x";
					//				d.DomainDotVn = "x";
					//				break;
					//			case 0:
					//				d.DomainDotVn = "x";
					//				break;
					//			case 1:
					//				d.DomainDotComDotVn = "x";
					//				break;
					//		}
					//	}

					//	lock(locker)
					//	{
					//		resultCheckCompare.Add(d);
					//	}
					//	this.BeginInvoke((Action)delegate { progressBar1.Value += 10; });
					////}
					//});


					this.BeginInvoke(mWriteRichTextBox, new object[] { "\n[End] Check Compare Domain", rtxtView });
					
				}

				if (resultCheckCompare.Count > 0 && allowExport)
				{
					_resultCheckCompare.Clear();
					_resultCheckCompare = resultCheckCompare;
					ProcessExportResultCheckCompore();
				}
				totalTile.Stop();
				this.BeginInvoke(mWriteRichTextBox, new object[] { "\n[Elapsed] Tổng thời gian: " + totalTile.Elapsed.TotalMinutes + " minutes", rtxtView });
			}
			catch (Exception ex)
			{
				this.BeginInvoke(mWriteRichTextBox, new object[] { "[Error] " + ex.ToString(), rtxtView });
				_bgwProcessBar.CancelAsync();
			}
		}

		private void ProcessExportResultCheckCompore()
		{
			try
			{
				Stopwatch watch = new Stopwatch();
				watch.Start();
				this.BeginInvoke(mWriteRichTextBox, new object[] { "\n----------[Begin] Export", rtxtView });

				//DataTable dt = _brandDomainService.ConvertListToDataTable(_resultCompare);

				if (_resultCheckCompare.Count <= 0)
				{
					this.BeginInvoke(mWriteRichTextBox, new object[] { "[Warning] Không có domain để export", rtxtView });
				}
				else
				{
					this.BeginInvoke(mWriteRichTextBox, new object[] { "Đang thực hiện export...", rtxtView });

					string filename = string.Format("ResultCheckCompare_{0}.xlsx", DateTime.Now.ToString("ddMMyyyyhhmmss"));
					if (!Directory.Exists(System.Environment.CurrentDirectory + "\\UserFile"))
					{
						Directory.CreateDirectory(System.Environment.CurrentDirectory + "\\UserFile");
					}

					string path = System.Environment.CurrentDirectory + "\\UserFile\\" + filename;

					if (CreateExcelFile.CreateExcelDocument<Domain>(_resultCheckCompare, path))
					{
						this.BeginInvoke(mWriteRichTextBox, new object[] { "[Success] " + filename, rtxtView });


						string windir = Environment.GetEnvironmentVariable("windir");
						if (string.IsNullOrEmpty(windir.Trim()))
						{
							windir = "C:\\Windows\\";
						}
						if (!windir.EndsWith("\\"))
						{
							windir += "\\";
						}

						FileInfo fileToLocate = null;
						fileToLocate = new FileInfo(path);

						ProcessStartInfo pi = new ProcessStartInfo(windir + "explorer.exe");
						pi.Arguments = "/select, \"" + fileToLocate.FullName + "\"";
						pi.WindowStyle = ProcessWindowStyle.Normal;
						pi.WorkingDirectory = windir;

						//Start Process
						Process.Start(pi);

						//MessageBox.Show("Export Success");


					}
					else
					{

						this.BeginInvoke(mWriteRichTextBox, new object[] { "[Error] Export error: \n", rtxtView });
					}

					//string name=string.Format("ResultCompare_{0}.xlsx",DateTime.Now.ToString("ddMMyyyyhhmmss"));
					//string error = "";

					//if (_brandDomainService.ExportResultCompareDomain(_resultCompare, name,ref error))
					//{
					//	this.BeginInvoke(mWriteRichTextBox, new object[] { "[Success] " + name, rtxtView });
					//}
					//else
					//{

					//	this.BeginInvoke(mWriteRichTextBox, new object[] { "[Error] Export error: \n" + error, rtxtView });
					//}
				}
				watch.Stop();
				this.BeginInvoke(mWriteRichTextBox, new object[] { "[Time] " + watch.Elapsed.TotalMinutes + " minutes", rtxtView });
				this.BeginInvoke(mWriteRichTextBox, new object[] { "[End] Export", rtxtView });
			}
			catch (Exception ex)
			{
				this.BeginInvoke(mWriteRichTextBox, new object[] { "[Error] Export error: \n" + ex.ToString(), rtxtView });
			}
		}
	}
}
