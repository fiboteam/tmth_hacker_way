﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessObjects
{
	public class GroupDomainVn2
	{
		public string Prefix { get; set; }
		public string DomainVn { get; set; }
		public string DomainComVn { get; set; }
		public int TotalDomain { get; set; }
		public bool HaveRange { get; set; }
		public int Begin { get; set; }
		public int End { get; set; }

		public GroupDomainVn2()
		{
			HaveRange = false;
			Begin = 0;
			End = 0;
		}

		public GroupDomainVn2(GroupDomainVn2 cpy)
		{
			this.Prefix = cpy.Prefix;
			this.DomainVn = cpy.DomainVn;
			this.DomainComVn = cpy.DomainComVn;
			this.TotalDomain = cpy.TotalDomain;
		}

	}
}
