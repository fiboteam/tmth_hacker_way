﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessObjects
{
	public class ObjectCompareDomainVN
	{
		public List<DomainVN> ListDomainVN { get; set; }
		public List<GroupDomainVN> ListGroupDomainVN { get; set; }
		public string StringPattern { get; set; }

		public ObjectCompareDomainVN()
		{
			ListDomainVN = new List<DomainVN>();
			ListGroupDomainVN = new List<GroupDomainVN>();
			StringPattern = "";
		}
	}
}
