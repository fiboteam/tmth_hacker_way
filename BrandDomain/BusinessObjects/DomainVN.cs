﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessObjects
{
    public class DomainVN
    {
		public string DomainName { get; set; }
		public string Prefix { get; set; }
		public DateTime? RegistrationDate { get; set; }
		public DateTime? ExpireDate { get; set; }
		public DateTime? AuctionDate { get; set; }
    }
}
