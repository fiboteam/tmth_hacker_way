﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessObjects
{
	public class Domain
	{
		public string DomainName { get; set; }
		public string DomainDotVn { get; set; }
		public string DomainDotComDotVn { get; set; }
		public Domain()
		{
			DomainDotVn = "";
			DomainDotComDotVn = "";
		}
	}
}
