﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utility;

namespace BusinessObjects
{
	public class GroupDomainVN
	{
		public string Name { get; set; }
		public EnumDomain TotalDomain { get; set; }
		
		public int Begin { get; set; }
		public int End { get; set; }
		public bool HaveRange { get; set; }
		public GroupDomainVN(GroupDomainVN copy)
		{
			this.Name = copy.Name;
			this.TotalDomain = copy.TotalDomain;

		}
		public GroupDomainVN()
		{
			this.TotalDomain = EnumDomain.NotVn;
		}
	}
}
