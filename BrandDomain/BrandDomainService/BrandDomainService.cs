﻿using BusinessObjects;
using DataAccessObjects;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace BrandDomainService
{
	public class BrandDomainService01:IBrandDomainService
	{
		private SqlDomainVNDao _SqlDomainVNDao = new SqlDomainVNDao();
		/// <summary>
		/// Đọc domain quốc tế từ file
		/// </summary>
		/// <param name="path"></param>
		public IList<Domain> GetDomainInternationalFromExcel(string path,int sheetIndex,int columnNameIndex)
		{
			List<Domain> domains = new List<Domain>();
			XSSFWorkbook wb;
			XSSFSheet sh;

			try
			{
				if(File.Exists(path))
				{
					// get file list from xlsx
					using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read))
					{
						wb = new XSSFWorkbook(fs);
					}

					// get sheet
					sh = (XSSFSheet)wb.GetSheetAt(sheetIndex);
					System.Collections.IEnumerator rows = sh.GetRowEnumerator();

					int i = 0;

					

					while(rows.MoveNext())
					{
						if (i > 0)
						{
							IRow row = (XSSFRow)rows.Current;
							if(row!=null)
							{
								ICell cellName = row.GetCell(columnNameIndex);
								string name = cellName != null ? cellName.ToString() : "";
								if(!string.IsNullOrEmpty(name))
								{
									Domain domain = new Domain();
									domain.DomainName = name.Trim();
									domain.DomainName = domain.DomainName.ToLower();

									domains.Add(domain);
								}
							}
						}
						i++;
					}
				}
				//return domains.OrderBy(order=>order.DomainName).ToList();
				//domains.Sort((s1,s2)=>s1.DomainName.CompareTo(s2.DomainName));
				domains.Sort((s1,s2)=>string.Compare(s1.DomainName,s2.DomainName,StringComparison.Ordinal));
				return domains; 
				//return domains;
			}
			catch
			{
				return domains;
			}

		}


		public ObjectCompareDomainVN GetListNameDomainVNWithFilter(DateTime expireDate)
		{
			return _SqlDomainVNDao.GetNameDomainVN(expireDate);
		}




		public bool ExportResultCompareDomain(List<Domain> domains,string filename,ref string stringError)
		{
			try
			{
				XSSFWorkbook wb = new XSSFWorkbook();

				// create sheet
				XSSFSheet sh = (XSSFSheet)wb.CreateSheet("Sheet1");

				var style1 = wb.CreateCellStyle();
				style1.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
				style1.FillPattern = FillPattern.SolidForeground;
				style1.FillBackgroundColor = NPOI.HSSF.Util.HSSFColor.OliveGreen.Index;

				IDataFormat dataFormatCustom = wb.CreateDataFormat();

				IRow rowheader = sh.CreateRow(0);
				int count = 0;

				rowheader.CreateCell(count).SetCellValue("Domain Quốc Tế");
				rowheader.GetCell(count).CellStyle = style1;
				count++;
				rowheader.CreateCell(count).SetCellValue("Domain .vn");
				rowheader.GetCell(count).CellStyle = style1;
				count++;
				rowheader.CreateCell(count).SetCellValue("Domain .com.vn");
				rowheader.GetCell(count).CellStyle = style1;
				count++;

				int row = 1;
				for (int i = 0; i < domains.Count; i++)
				{
					if (sh.GetRow(row) == null)
						sh.CreateRow(row);

					int x = 0;
					sh.GetRow(row).CreateCell(x).SetCellValue(domains[i].DomainName);
					x++;
					sh.GetRow(row).CreateCell(x).SetCellValue(domains[i].DomainDotVn);
					x++;
					sh.GetRow(row).CreateCell(x).SetCellValue(domains[i].DomainDotComDotVn);
					x++;

					row++;
				}


				MemoryStream file = new MemoryStream();
				wb.Write(file);

				if(!Directory.Exists(System.Environment.CurrentDirectory + "\\UserFile"))
				{
					Directory.CreateDirectory(System.Environment.CurrentDirectory + "\\UserFile");
				}

				string path = System.Environment.CurrentDirectory + "\\UserFile\\" + filename;

				FileStream files = new FileStream(path, FileMode.Create, FileAccess.ReadWrite);
				files.Write(file.GetBuffer(), 0, (int)file.GetBuffer().Length);

				files.Close();
				file.Close();

				if (!path.StartsWith("System.ArgumentException"))
				{
					try
					{
						string windir = Environment.GetEnvironmentVariable("windir");
						if (string.IsNullOrEmpty(windir.Trim()))
						{
							windir = "C:\\Windows\\";
						}
						if (!windir.EndsWith("\\"))
						{
							windir += "\\";
						}

						FileInfo fileToLocate = null;
						fileToLocate = new FileInfo(path);

						ProcessStartInfo pi = new ProcessStartInfo(windir + "explorer.exe");
						pi.Arguments = "/select, \"" + fileToLocate.FullName + "\"";
						pi.WindowStyle = ProcessWindowStyle.Normal;
						pi.WorkingDirectory = windir;

						//Start Process
						Process.Start(pi);

						//MessageBox.Show("Export Success");
					}
					catch (Exception ex)
					{
						//MessageBox.Show(ex.ToString());
					}
				}
				else
				{
					//MessageBox.Show(myPath);
				}


				//byte[] byteArray = file.ToArray();
				//file.Flush();

				//file.Close();
				


				return true;
			}
			catch(Exception ex)
			{
				stringError = ex.ToString();
				return false;
			}
		}

		public List<GroupDomainVn2> GetGroupDomainVn(DateTime dateTime)
		{
			return _SqlDomainVNDao.GetGroupDomainVn(dateTime);
		}

		public DataTable ConvertListToDataTable(List<Domain> listDomains)
		{
			try
			{
				DataTable dt = new DataTable();
				dt.Columns.Add("Domain Quốc Tế", typeof(string));
				dt.Columns.Add("Domain .com.vn", typeof(string));
				dt.Columns.Add("Domain .vn", typeof(string));

				Parallel.ForEach(listDomains, domain =>
				{
					dt.Rows.Add(domain.DomainName, domain.DomainDotComDotVn, domain.DomainDotVn);
				});
				return dt;
			}
			catch
			{
				return null;
			}
		}


		public int CheckCompareDomain(DateTime datime, string domain)
		{
			string domain01 = @"\.vn$";
			Regex regex01 = new Regex(domain01);
			string domain02 = @"\.com\.vn$";
			Regex regex02 = new Regex(domain02);

			var list= _SqlDomainVNDao.CheckCompareDomain(datime, domain);

			if (list.Count <= 0)
			{
				return -1;
			}
			else if (list.Count == 2)
			{
				return 2;
			}
			else if (regex02.IsMatch(list[0]))
			{
				return 1;
			}
			else if (regex01.IsMatch(list[0]))
			{
				return 0;
			}
			else
				return -2;
		}
	}
}
