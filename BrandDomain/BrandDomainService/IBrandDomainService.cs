﻿using BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandDomainService
{
	public interface IBrandDomainService
	{
		IList<Domain> GetDomainInternationalFromExcel(string path, int sheetIndex, int columnNameIndex);

		bool ExportResultCompareDomain(List<Domain> domains, string filename, ref string stringError);

		ObjectCompareDomainVN GetListNameDomainVNWithFilter(DateTime expireDate);
		List<GroupDomainVn2> GetGroupDomainVn(DateTime dateTime);

		int CheckCompareDomain(DateTime datime, string domain);
	}
}
