﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Utility
{
	public class SelectItem
	{
		public string Name { get; set; }
		public int Value { get; set; }
	}

    public static class GFunction
    {
		
		public static string GetConnectionStr(string name)
		{
			return ConfigurationManager.ConnectionStrings[name].ConnectionString;
			
		}

		/// <summary>
		/// NMH
		/// Chuyển enum to list 
		/// </summary>
		/// <typeparam name="T1"></typeparam>
		/// <returns></returns>
		public static List<SelectItem> ConverEnumToListItem<TEnum>(bool insertFist = false) where TEnum : struct
		{

			//var dictionary = new Dictionary<short, string>();
			List<SelectItem> items = new List<SelectItem>();

			if (insertFist)
			{
				//dictionary.Add(0, "All");
				items.Add(new SelectItem() { Name = "All", Value = 0 });
			}

			var enumerationType = typeof(TEnum);

			if (!enumerationType.IsEnum)
			{
				//return dictionary;
				return items;
			}

			try
			{
				foreach (short value in Enum.GetValues(enumerationType))
				{
					var name = Enum.GetName(enumerationType, value);
					name = name.Replace("_", " ");
					//dictionary.Add(value, name);
					items.Add(new SelectItem() { Name = name, Value = value });
				}
			}
			catch
			{
				return items;
			}

			return items;
		}
    }
}
